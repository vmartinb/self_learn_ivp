## Files

main: file to do self-learning using IVP

main_noisy: file to do self-learning using IVP on noisy datasets

main_pairwise: file to do self-learning using pairwise IVAP on one-vs-one approach

main_pairwise_ova: file to do self-learning using pairwise IVAP on one-vs-all approach

parserml: configurations of the training

datasets: functions to load each dataset

neural_net: neural network used

requirements: python packages used

