import torch
from neural_net import ConvNet,pred_per_epoch
from tensorflow import keras
import numpy as np
from sklearn.model_selection import train_test_split
import pandas as pd
import matplotlib.pyplot as plt
from taxonomies_calibration import *

def one_hot(vector,num_classes=10):
    vector = vector.squeeze()
    b = np.zeros((vector.size,num_classes)).astype(np.int64)
    b[np.arange(vector.size), vector] = 1
    return b


(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()
y_train = y_train.reshape((-1,1))
y_test = y_test.reshape((-1,1))
y = np.vstack((y_train,y_test))
X = np.vstack((x_train,x_test))
y = one_hot(y)
num = 1 if X.shape[0]>200 else 10
seed = np.linspace(start=1,stop=100,num=num)[0].astype(int)
split = 11000

X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=.2,random_state=seed)
prop_per_class = split*np.unique(np.argmax(y_train,axis=1),return_counts=True)[1]/len(y_train)
prop_per_class = (prop_per_class/np.min(prop_per_class)).astype(int)
indexes = []
X_labeled,y_labeled = X_train[:split],y_train[:split]
X_unlabeled = X_train[split:]

device = torch.device('cpu')
#sety = y_unlabeled
setx = X_unlabeled
samples=40
iterations = np.linspace(start=0,stop=49,num=10).astype(int)#[8,15,20,22]
print(iterations)
#prints = {'true label':sety.tolist()}
samples = np.linspace(start=0,stop=1000,num=30)
prints = {'iterations':iterations}
for k in range(len(samples)):
    prints[str(k)+'_upper'] = []
    prints[str(k)+'_lower'] = []
rep_per_class=10
for cl in range(10):
        indexes.append(np.where(y_labeled[:,cl]==1)[0][:rep_per_class])
indexes = np.concatenate(indexes)
X_cal,y_cal = X_labeled[indexes],y_labeled[indexes]
X_labeled = np.delete(X_labeled,indexes,0)
y_labeled = np.delete(y_labeled,indexes,0)
for it in iterations:
    pred_cal,label = pred_per_epoch(it,X_cal,st='credal')
    y_unlabeled,label = pred_per_epoch(it,X_unlabeled)
    for k in range(len(samples)):
        prints[str(k)+'_upper'].append([])
        prints[str(k)+'_lower'].append([])
        venn_pred = venn_prediction(y_unlabeled[k],y_cal,pred_cal,nn_v1)
        upper = np.max(venn_pred,axis=0)
        lower = np.min(venn_pred,axis=0)
        print('*'*50)
        print('iteration:{}'.format(it))
        print(upper)
        print(lower)
        prints[str(k)+'_upper'][-1].append([upper])
        prints[str(k)+'_lower'][-1].append([lower])
write = pd.DataFrame(prints)
print(write)
write.to_excel('mnist/credal_sets.xlsx')
'''
    pred,label = pred_per_epoch(it,setx)
    list_arr= np.hstack((np.max(pred,axis=1).reshape((-1,1)),label)).tolist()
    prints[str(it)]= [ [round(elem[0], 2),int(elem[1])] for elem in list_arr]
    plt.scatter(np.arange(samples),label[:samples])
    unique,counts=np.unique(label,return_counts=True)
    counts = counts/np.sum(counts)
    print(np.asarray((unique,counts)).T)
plt.scatter(np.arange(samples),sety[:samples])
plt.legend(iterations.tolist()+['true label'])
write = pd.DataFrame(prints)
print(write)
write.to_excel('mnist/comparacao.xlsx')
plt.savefig('mnist/comparacao_label')

def func(pred):
    return np.mean(np.max(pred,axis=1))

plt.figure()
for st in ['hard','soft','credal']:
    iterations = np.arange(1,48)
    pred,label = pred_per_epoch(0,setx)
    meanmax = func(pred)
    for it in iterations:
        pred,label = pred_per_epoch(it,setx,st)
        meanmax = np.hstack((meanmax,func(pred)))
    plt.plot(np.arange(48),meanmax)
plt.legend(['hard','soft','credal'])
plt.savefig('mnist/comparacao')
'''