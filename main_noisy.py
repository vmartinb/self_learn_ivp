import sys
sys.path.append('/home/vitorbordini/Downloads/self_learn_ivp')
import wandb
import pandas as pd
import numpy as np
from sklearn.metrics import accuracy_score,confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler,normalize
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.utils import shuffle
import matplotlib.pyplot as plt
import seaborn as sns
from multiprocessing import Pool
import xlsxwriter
from tqdm import tqdm

import torch
from torch import nn
from torch.nn import functional as F
from torch.autograd import Variable
from collections import OrderedDict
import tensorflow as tf
import cvxpy as cp

import os

from neural_net import SimpleNeuralNet,ConvNet
from utils import expected_calibration_error,asymmetric_noise_for_dataset,symmetric_noise,dataset,solve_cvx,one_hot
from taxonomies_calibration import *
from optimistic_solver import solve
from parserml import argpar



wandb.login()
par = argpar()
device = 'cuda:'+ par.cuda if par.cuda!=-1 else 'cpu'
noise_st = 'min' if par.sym_noise else 'classwise'
data,n_hidden_units = dataset(par)

run = wandb.init(
    # Set the project where this run will be logged
    project="my-awesome-project",
    # Track hyperparameters and run metadata
    config={
        "learning_rate": par.lr,
        "dataset": par.dataset,
        "rep_per_class":par.rep_per_class,
        "labeled_instances": 30
    })

X,y = data['data'],data['target']
#X = normalize(X)
num_classes = np.max(y)+1

n_input = X.shape[1]
y = one_hot(y,num_classes=num_classes).squeeze()
#X,y = shuffle(X,y)
'''
if X.shape[1]==2:
    for label in range(num_classes):
        indexes = np.where(y[:,label]==1)[0]
        plt.scatter(X[indexes,0],X[indexes,1],label='class_{}'.format(label))

        
    plt.legend()
    plt.savefig('dataset')
'''


split = int(par.split_size*X.shape[0])
def self_learn(st='soft'):
    '''
    Self-training algorithm
        Args:
            X(ndarray, shape (N,M)): inputs
            y(ndarray, shape (N,num_classes)): outputs (hard labels)
            taxonomy(function): Venn taxonomy in case of IVP.
            lr(float): learning rate for training.
            st(string): Strategy implemented.
            epochs(int): Number of epochs to train the model on each iteration.
            iterations(int): Number of maximum iterations.
    '''
    lr = par.lr
    iterations=50
    epochs=50
    
    num = 10 if X.shape[0]>200 else 10
    seeds = np.linspace(start=1,stop=100,num=num).astype(int)#[2,13,25,42,57,64,78,85,96]
    num_classes = y.shape[1]
    accuracies = np.zeros((iterations,len(seeds)))
    eces = np.zeros((iterations,len(seeds)))
    accuracies_per_class = np.zeros((iterations,len(seeds),num_classes))
    rep_per_class= par.rep_per_class
    print(X.shape)


    for i,seed in enumerate(seeds):
        X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=.2,random_state=seed)
        prop_per_class = split*np.unique(np.argmax(y_train,axis=1),return_counts=True)[1]/len(y_train)
        prop_per_class = (prop_per_class/np.min(prop_per_class)).astype(int)
        if par.im_data:
            model = ConvNet(clipping_value=0.01,dataset=par.dataset,device=device,num_classes=num_classes)
        else:
            model = SimpleNeuralNet(0.01,n_input,n_hidden_units,num_classes)
        indexes = []
        for cl in range(num_classes):
            indexes.append(np.where(y_train[:,cl]==1)[0][:prop_per_class[cl]])
        indexes = np.concatenate(indexes)
        X_labeled,y_labeled = X_train[indexes],y_train[indexes]
        X_unlabeled = np.delete(X_train,indexes,0)
        y_unlabeled = np.delete(y_train,indexes,0)
        X_labeled,y_labeled = shuffle(X_labeled,y_labeled)

        X_labeled,y_labeled = X_train[:split],y_train[:split]
        X_unlabeled = X_train[split:]
        prop = int(len(X_unlabeled)/iterations)
        indexes = []
        for cl in range(num_classes):
                indexes.append(np.where(y_labeled[:,cl]==1)[0][:rep_per_class])
        indexes = np.concatenate(indexes)
        X_cal,y_cal = X_labeled[indexes],y_labeled[indexes]
        X_labeled = np.delete(X_labeled,indexes,0)
        y_labeled = np.delete(y_labeled,indexes,0)
        X_cal,y_cal = shuffle(X_cal,y_cal)
        noise_func = symmetric_noise if par.sym_noise else asymmetric_noise_for_dataset

        y_labeled = one_hot(noise_func(par,X_test,np.argmax(y_test,axis=1),par.p,num_classes,X_labeled,np.argmax(y_labeled,axis=1),seed),num_classes)

        if st!='credal':
            y_labeled = np.vstack((y_labeled,y_cal))
            X_labeled = np.vstack((X_labeled,X_cal))

        model.fit(X_labeled,y_labeled,epochs,lr,verbose=False)
        threshold=0.8
        for j in tqdm(range(iterations)):
            if st =='hard':
                y_unlabeled = model.predict_proba(X_unlabeled,as_numpy=True)
                probs_to_add = np.max(y_unlabeled,axis=1)>threshold
                
                y_labeled = np.vstack((y_labeled,y_unlabeled[probs_to_add]))
                y_unlabeled = y_unlabeled[np.logical_not(probs_to_add)]
                X_labeled = np.vstack((X_labeled,X_unlabeled[probs_to_add]))
                y_unlabeled = one_hot(np.argmax(y_unlabeled,axis=1),num_classes)
                X_unlabeled = X_unlabeled[np.logical_not(probs_to_add)]
                y_acc = y_labeled#torch.vstack((y_labeled,y_unlabeled))
                X_acc = X_labeled#torch.vstack((X_labeled,X_unlabeled[j*prop:(j+1)*prop]))
                soft=False

            elif st=='credal':
                y_unlabeled = model.predict_proba(X_unlabeled)
                epsilon = 0.01
                probs_to_add = np.max(y_unlabeled,axis=1)>threshold
                
                y_probs = y_unlabeled[probs_to_add].copy()
                X_probs = X_unlabeled[probs_to_add].copy()
                X_unlabeled = X_unlabeled[np.logical_not(probs_to_add)]
                y_unlabeled = y_unlabeled[np.logical_not(probs_to_add)]
                params_list = []
                pred_cal = model.predict_proba(X_cal)
                for k in range(len(X_probs)):
                    params_list.append(dict())
                    venn_pred = venn_prediction(y_probs[k],y_cal,pred_cal,nn_v1)
                    upper = np.max(venn_pred,axis=0)
                    lower = np.min(venn_pred,axis=0)
                    upper = (1-epsilon)*upper + epsilon/num_classes
                    lower = (1-epsilon)*lower + epsilon/num_classes
                    params_list[-1]['upper'] = upper
                    params_list[-1]['lower'] = lower
                    params_list[-1]['pred'] = y_probs[k].copy()
                    params_list[-1]['option'] = 'min'
                    p_opt = solve_cvx(params_list[-1])
                    y_probs[k] = p_opt
                    if num_classes==2:
                        y_probs[k,1] =  upper[1] if y_probs[k,1] >= upper[1] else y_probs[k,1]
                        y_probs[k,1] =  lower[1] if y_probs[k,1] <= lower[1] else y_probs[k,1]

                soft = True
                y_acc = np.vstack((y_labeled,y_probs))
                y_labeled = np.vstack((y_labeled,y_probs))
                X_labeled = np.vstack((X_labeled,X_probs))
                X_acc = np.vstack((X_labeled,X_probs))
            elif st == 'soft':
                y_unlabeled = model.predict_proba(X_unlabeled)            	
                probs_to_add = np.max(y_unlabeled,axis=1)>threshold
                y_labeled = np.vstack((y_labeled,y_unlabeled[probs_to_add]))
                y_unlabeled = y_unlabeled[np.logical_not(probs_to_add)]
                X_labeled = np.vstack((X_labeled,X_unlabeled[probs_to_add]))
                X_unlabeled = X_unlabeled[np.logical_not(probs_to_add)]
                y_acc = y_labeled#np.vstack((y_labeled,y_unlabeled))
                X_acc = X_labeled#np.vstack((X_labeled,X_unlabeled))
                soft=True
            model.fit(X_acc,y_acc,epochs,lr,verbose=False,soft=soft)
            test_pred = model.predict_proba(X_test,as_numpy=False).float().detach().numpy()
            if num_classes>2:
                matrix = confusion_matrix(y_true=np.argmax(y_test,axis=1),
                                          y_pred=np.argmax(test_pred,axis=1))
            else:
                matrix = confusion_matrix(y_true=np.argmax(y_test,axis=1),
                                          y_pred=(test_pred>0.5).astype(int))
            accuracies[j,i] = np.sum(np.diag(matrix))/np.sum(matrix)
            eces[j,i] =  expected_calibration_error(test_pred,y_test,num_classes=num_classes)
            accuracies_per_class[j,i,:]=(matrix.astype('float') / matrix.sum(axis=1)[:, np.newaxis]).diagonal()
    mean_accuracies = np.mean(accuracies,axis=1).astype('<U5')
    std_accuracies =  np.std(accuracies,axis=1).astype('<U5')
    mean_eces = np.mean(eces,axis=1).astype('<U5')
    std_eces = np.std(eces,axis=1).astype('<U5')
    sep=np.array(['+/-']*len(mean_accuracies))
    print_accuracies = np.char.add(np.char.add(mean_accuracies,sep),std_accuracies)
    print_eces = np.char.add(np.char.add(mean_eces,sep),std_eces)
    with pd.ExcelWriter('noisy/{}/result_{}_{}_{}_{}.xlsx'.format(par.dataset,par.rep_per_class*num_classes,split,st,noise_st), engine='xlsxwriter') as writer:
        pd.DataFrame(print_accuracies).to_excel(writer,sheet_name='accuracies')
        pd.DataFrame(print_eces).to_excel(writer,sheet_name='eces')

    return np.mean(accuracies,axis=1),np.mean(accuracies_per_class,axis=1),np.mean(eces,axis=1)

def compare_self_learn():
    if not os.path.exists('noisy'):
        os.mkdir('noisy')
    if not os.path.exists('noisy/' + par.dataset):
        os.mkdir('noisy/' + par.dataset)
    '''
    Employs three different strategies to solve self-learning problem.
    '''
    acc_cred,acc_per_class_cred,eces_credal = self_learn('credal')
    print(acc_cred)
    acc_soft,acc_per_class_soft,eces_soft = self_learn('soft')
    print(acc_soft)
    acc_hard,acc_per_class_hard,eces_hard = self_learn('hard')
    print(acc_hard)
    
    
    
    plt.figure()
    plot_dict = {'hard':acc_hard, 'soft':acc_soft, 'credal_opt':acc_cred}
    plot = pd.DataFrame(plot_dict)
    wandb.log(plot_dict)
    sns.lineplot(data=plot).set(title="Accuracy")
    plt.savefig('noisy/{}/result_{}_{}_{}_{}_acc'.format(par.dataset,par.rep_per_class*num_classes,split,int(par.p*100),noise_st))

    plt.figure()
    plot_dict = {'hard':eces_hard, 'soft':eces_soft, 'credal_opt':eces_credal}
    plot = pd.DataFrame(plot_dict)
    wandb.log(plot_dict)
    sns.lineplot(data=plot).set(title="ECE")
    plt.savefig('noisy/{}/result_{}_{}_{}_{}_ece'.format(par.dataset,par.rep_per_class*num_classes,split,int(par.p*100),noise_st))

    for cl in range(num_classes):
        plt.figure()
        plot_dict_class = {'hard_'+str(cl):acc_per_class_hard[:,cl], 'soft_'+str(cl):acc_per_class_soft[:,cl], 'credal_opt_'+str(cl):acc_per_class_cred[:,cl]}
        wandb.log(plot_dict_class)
        plot = pd.DataFrame(plot_dict_class)
        sns.lineplot(data=plot).set(title="Accuracy")
        plt.savefig('noisy/{}/result_{}_class_{}_{}_{}'.format(par.dataset,par.rep_per_class*num_classes,cl,noise_st,int(par.p*100)))

compare_self_learn()

def prob2xy(p1,p2,p3):
    return p1,p2#[p3/(1-p3)*p2+(1-p3)/2,(1-p3)/2]
    

def plot_point(bounds,predictions,p_opt,i):
    tri = np.array([[0,0],[1,0],[0,1]])
    
    upper = np.max(bounds,axis=0)
    lower = np.min(bounds,axis=0)
    
    plt.figure()
    
    plt.plot(tri[:-1,0],tri[:-1,1],color='blue',label='Domain')
    plt.plot(tri[1:,0],tri[1:,1],color='blue')
    plt.plot([tri[0,0],tri[2,0]],[tri[0,1],tri[2,1]],color='blue')
    
    x_pred,y_pred = prob2xy(predictions[0],predictions[1],predictions[2])
    
    point1 = prob2xy(upper[0],lower[1],lower[2])
    point2 = prob2xy(lower[0],upper[1],lower[2])
    point3 = prob2xy(lower[0],lower[1],upper[2])
    
    p_point= prob2xy(p_opt[0],p_opt[1],p_opt[2])
    
    tri_upper = np.array([point1,point2,point3])
    
    plt.scatter(tri_upper[:, 0], tri_upper[:, 1])
    
        
    t2 = plt.Polygon(tri_upper, color='red',label='Credal Set')
    plt.gca().add_patch(t2)
    
    plt.scatter([x_pred],[y_pred],s=100,label='Prediction')
    plt.scatter([p_point[0]],[p_point[1]],s=100,label='Optimal p')
    
    plt.legend()
    plt.savefig('notebooks/point_img/img_'+str(i))
    #plt.xlim([0,1])
    #plt.ylim([0,0.6])
#plot_point(venn_pred,predictions,p_opt)

'''
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=.33,random_state=96)
split = 100
X_labeled,y_labeled = X_train[:split],y_train[:split]
rep_per_class=7
indexes = np.zeros((num_classes*rep_per_class)).astype(int)
for cl in range(num_classes):
    indexes[cl*rep_per_class:(cl+1)*rep_per_class] = np.where(y_labeled[:,cl]==1)[0][:rep_per_class]
X_cal,y_cal = X_labeled[indexes],y_labeled[indexes]
X_labeled = np.delete(X_labeled,indexes,0)
y_labeled = np.delete(y_labeled,indexes,0)
X_cal,y_cal = shuffle(X_cal,y_cal)

predictions = clf.predict_proba(X_test)
for i in range(len(X_test)):
    venn_pred = venn_prediction(X_test[i],X_cal,y_cal,clf,nn_v1)
    upper = np.max(venn_pred,axis=0)
    lower = np.min(venn_pred,axis=0)
    if all(np.logical_and(predictions[i]<upper,predictions[i]>lower)):
        p_opt=predictions[i]
    else:
        _,p_opt =solve_cvx(upper,lower,predictions[i],np)
    plot_point(venn_pred,predictions[i],p_opt,i)    
'''
