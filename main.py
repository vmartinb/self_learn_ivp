import sys
sys.path.append('/home/vitorbordini/Downloads/self_learn_ivp')
#import wandb
import pandas as pd
import numpy as np
from sklearn.metrics import accuracy_score,confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler,normalize
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.utils import shuffle
import matplotlib.pyplot as plt
import seaborn as sns
import xlsxwriter

import torch
from torch import nn
from torch.nn import functional as F
from torch.autograd import Variable
from collections import OrderedDict
import tensorflow as tf
from multiprocessing import Pool
import os

from neural_net import SimpleNeuralNet,ConvNet
from utils import expected_calibration_error,dataset,solve_cvx
from taxonomies_calibration import *
from optimistic_solver import solve
from parserml import argpar
from tqdm import tqdm
def one_hot(vector,num_classes=3):
    vector = vector.squeeze()
    b = np.zeros((vector.size,num_classes)).astype(np.int64)
    b[np.arange(vector.size), vector] = 1
    return b

#wandb.login()
par = argpar()
device = 'cuda:'+ par.cuda if par.cuda!=-1 else 'cpu'
data,n_hidden_units = dataset(par)
print(par)
'''
run = wandb.init(
#    mode="disabled",
    # Set the project where this run will be logged
    project="my-awesome-project",
    # Track hyperparameters and run metadata
    config={
        "learning_rate": par.lr,
        "dataset": par.dataset,
        "rep_per_class":par.rep_per_class,
        "labeled_instances": 20
    })
'''
X,y = data['data'],data['target']
if not par.im_data:
    X = normalize(X)
num_classes = np.max(y)+1

n_input = X.shape[1]
y = one_hot(y,num_classes=num_classes).squeeze()
#X,y = shuffle(X,y)
'''
if X.shape[1]==2:
    for label in range(num_classes):
        indexes = np.where(y[:,label]==1)[0]
        plt.scatter(X[indexes,0],X[indexes,1],label='class_{}'.format(label))

        
    plt.legend()
    plt.savefig('dataset')
'''

split = int(par.split_size*X.shape[0])   
def self_learn(st='soft'):
    '''
    Self-training algorithm
        Args:
            X(ndarray, shape (N,M)): inputs
            y(ndarray, shape (N,num_classes)): outputs (hard labels)
            taxonomy(function): Venn taxonomy in case of IVP.
            lr(float): learning rate for training.
            st(string): Strategy implemented.
            epochs(int): Number of epochs to train the model on each iteration.
            iterations(int): Number of maximum iterations.
    '''
    lr = par.lr
    iterations=50
    epochs=50
    
    num = 10 if X.shape[0]>200 else 10
    seeds = np.linspace(start=1,stop=100,num=num).astype(int)
    num_classes = y.shape[1]
    accuracies = np.zeros((iterations,len(seeds)))
    eces = np.zeros((iterations,len(seeds)))
    accuracies_per_class = np.zeros((iterations,len(seeds),num_classes))
    rep_per_class= par.rep_per_class
    loss_value_test = np.array([])
    loss_value_train = np.array([])
    print(X.shape)
    for i,seed in enumerate(seeds):
        X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=.2,random_state=seed)


        prop_per_class = split*np.unique(np.argmax(y_train,axis=1),return_counts=True)[1]/len(y_train)
        prop_per_class = (prop_per_class/np.min(prop_per_class)).astype(int)
        torch.manual_seed(seed)
        if par.im_data:
            model = ConvNet(0.01,par.dataset,device,num_classes)
        else:
            model = SimpleNeuralNet(0.01,n_input,n_hidden_units,num_classes)
        indexes = []
        for cl in range(num_classes):
            indexes.append(np.where(y_train[:,cl]==1)[0][:prop_per_class[cl]])
        indexes = np.concatenate(indexes)
        X_labeled,y_labeled = X_train[indexes],y_train[indexes]
        X_unlabeled = np.delete(X_train,indexes,0)
        y_unlabeled = np.delete(y_train,indexes,0)
        X_labeled,y_labeled = shuffle(X_labeled,y_labeled)

        X_labeled,y_labeled = X_train[:split],y_train[:split]
        X_unlabeled = X_train[split:]
        y_unlabeled = y_train[split:]


        prop = int(len(X_unlabeled)/iterations)
        if st == 'credal':
            indexes = []
            for cl in range(num_classes):
                    indexes.append(np.where(y_labeled[:,cl]==1)[0][:rep_per_class])
            indexes = np.concatenate(indexes)
            X_cal,y_cal = X_labeled[indexes],y_labeled[indexes]
            X_labeled = np.delete(X_labeled,indexes,0)
            y_labeled = np.delete(y_labeled,indexes,0)
            X_cal,y_cal = shuffle(X_cal,y_cal)

        threshold=0.8
        model.fit(X_labeled,y_labeled,epochs,lr,verbose=False)
        for j in tqdm(range(iterations)):            
            if st =='hard':
                y_unlabeled = model.predict_proba(X_unlabeled,as_numpy=True)
                probs_to_add = np.max(y_unlabeled,axis=1)>threshold
                
                y_labeled = np.vstack((y_labeled,y_unlabeled[probs_to_add]))
                y_unlabeled = y_unlabeled[np.logical_not(probs_to_add)]
                X_labeled = np.vstack((X_labeled,X_unlabeled[probs_to_add]))
                y_unlabeled = one_hot(np.argmax(y_unlabeled,axis=1),num_classes)
                X_unlabeled = X_unlabeled[np.logical_not(probs_to_add)]
                y_acc = y_labeled#torch.vstack((y_labeled,y_unlabeled))
                X_acc = X_labeled#torch.vstack((X_labeled,X_unlabeled[j*prop:(j+1)*prop]))
                soft=False

            elif st=='credal':
                y_unlabeled = model.predict_proba(X_unlabeled)
                epsilon = 0.01
                probs_to_add = np.max(y_unlabeled,axis=1)>threshold
                
                y_probs = y_unlabeled[probs_to_add].copy()
                X_probs = X_unlabeled[probs_to_add].copy()
                X_unlabeled = X_unlabeled[np.logical_not(probs_to_add)]
                y_unlabeled = y_unlabeled[np.logical_not(probs_to_add)]
                params_list = []
                upper_list = []
                lowerr_list = []
                pred_cal = model.predict_proba(X_cal)
                for k in range(len(X_probs)):
                    params_list.append(dict())
                    venn_pred = venn_prediction(y_probs[k],y_cal,pred_cal,nn_v1)
                    upper = np.max(venn_pred,axis=0)
                    lower = np.min(venn_pred,axis=0)
                    upper = (1-epsilon)*upper + epsilon/num_classes
                    lower = (1-epsilon)*lower + epsilon/num_classes
                    params_list[-1]['upper'] = upper
                    params_list[-1]['lower'] = lower
                    params_list[-1]['pred'] = y_probs[k].copy()
                    params_list[-1]['option'] = 'min'
                    if num_classes==2:
                        y_probs[k,1] = lower[1] if y_probs[k,1]<=lower[1] else y_probs[k,1]
                        y_probs[k,1] = upper[1] if y_probs[k,1]>=upper[1] else y_probs[k,1]                    
              
                if num_classes > 2:
                    processes = 50
                    p = Pool(processes)
                    for k in range(len(X_probs)//processes):
                        results = p.map(solve_cvx,params_list[k*processes:(k+1)*processes])
                        y_probs[k*processes:(k+1)*processes]=results
                soft = True
                y_acc = np.vstack((y_labeled,y_probs))
                y_labeled = np.vstack((y_labeled,y_probs))
                X_labeled = np.vstack((X_labeled,X_probs))
                X_acc = np.vstack((X_labeled,X_probs))
                
            elif st == 'soft':
                y_unlabeled = model.predict_proba(X_unlabeled)
                probs_to_add = np.max(y_unlabeled,axis=1)>threshold
                y_labeled = np.vstack((y_labeled,y_unlabeled[probs_to_add]))
                y_unlabeled = y_unlabeled[np.logical_not(probs_to_add)]
                X_labeled = np.vstack((X_labeled,X_unlabeled[probs_to_add]))
                X_unlabeled = X_unlabeled[np.logical_not(probs_to_add)]
                y_acc = y_labeled#np.vstack((y_labeled,y_unlabeled))
                X_acc = X_labeled#np.vstack((X_labeled,X_unlabeled))
                soft=True


            test_pred = model.predict_proba(X_test,as_numpy=False).float().cpu().detach().numpy()
            model.fit(X_acc,y_acc,epochs,lr,verbose=False,soft=soft)
            y_test_nonzero = y_test.copy().astype(np.float64)
            y_test_nonzero[y_test_nonzero==0]+= 0.001
            y_test_nonzero[y_test_nonzero==1]-= 0.001
            loss_value_test = np.hstack((loss_value_test,(y_test_nonzero*(np.log(y_test_nonzero)-np.log(test_pred))).sum(axis=1).mean()))
            plt.plot(loss_value_test)
            plt.savefig('{}/loss_test_{}_min'.format(par.dataset,st))
            plt.clf()
            
            y_train_nonzero = y_train.copy().astype(np.float64)
            y_train_nonzero[y_train_nonzero==0]+= 0.001
            y_train_nonzero[y_train_nonzero==1]-= 0.001
            train_pred = model.predict_proba(X_train,as_numpy=False).float().cpu().detach().numpy()
            loss_value_train = np.hstack((loss_value_train,(y_train_nonzero*(np.log(y_train_nonzero)-np.log(train_pred))).sum(axis=1).mean()))
            plt.plot(loss_value_train)
            plt.savefig('{}/loss_train_{}_min'.format(par.dataset,st))
            plt.clf()
            torch.save(model.state_dict(),'{}/model_{}_{}_{}_{}_min.pth'.format(par.dataset,par.rep_per_class*num_classes,split,j,st))

            if num_classes>2:
                matrix = confusion_matrix(y_true=np.argmax(y_test,axis=1),
                                          y_pred=np.argmax(test_pred,axis=1))
            else:
                matrix = confusion_matrix(y_true=np.argmax(y_test,axis=1).flatten(),
                                          y_pred=(test_pred>0.5).astype(int).flatten())
            accuracies[j,i] = np.sum(np.diag(matrix))/np.sum(matrix)
            eces[j,i] =  expected_calibration_error(test_pred,y_test,num_classes=num_classes)

            accuracies_per_class[j,i,:]=(matrix.astype('float') / matrix.sum(axis=1)[:, np.newaxis]).diagonal()

    mean_accuracies = np.mean(accuracies,axis=1).astype('<U5')
    std_accuracies =  np.std(accuracies,axis=1).astype('<U5')
    mean_eces = np.mean(eces,axis=1).astype('<U5')
    std_eces = np.std(eces,axis=1).astype('<U5')
    sep=np.array(['+/-']*len(mean_accuracies))
    print_accuracies = np.char.add(np.char.add(mean_accuracies,sep),std_accuracies)
    print_eces = np.char.add(np.char.add(mean_eces,sep),std_eces)
    with pd.ExcelWriter('{}/result_{}_{}_{}_min.xlsx'.format(par.dataset,par.rep_per_class*num_classes,split,st), engine='xlsxwriter') as writer:
        pd.DataFrame(print_accuracies).to_excel(writer,sheet_name='accuracies')
        pd.DataFrame(print_eces).to_excel(writer,sheet_name='eces')
    return np.mean(accuracies,axis=1),np.mean(accuracies_per_class,axis=1),np.mean(eces,axis=1)


def compare_self_learn():
    if not os.path.exists(par.dataset):
        os.mkdir(par.dataset)
    '''
    Employs three different strategies to solve self-learning problem.
    '''
    #results_cred,results_soft,results_hard = p.map(self_learn,['credal','soft',])
    acc_cred,acc_per_class_cred,eces_credal = self_learn('credal')
    print(acc_cred)
    acc_soft,acc_per_class_soft,eces_soft = self_learn('soft')
    print(acc_soft)
    acc_hard,acc_per_class_hard,eces_hard = self_learn('hard')
    print(acc_hard)

    num_classes = y.shape[1]
    
    plt.figure()
    plot_dict = {'hard':acc_hard, 'soft':acc_soft, 'credal_opt':acc_cred}
    #plot_dict_std = {'hard':std_acc_hard, 'soft':std_acc_soft, 'credal_opt':std_acc_credal}
    plot = pd.DataFrame(plot_dict)
    #wandb.log(plot_dict)
    sns.lineplot(data=plot).set(title="Accuracy")
    plt.savefig('{}/result_{}_{}_min'.format(par.dataset,par.rep_per_class*num_classes,split))


    plt.figure()
    plot_dict = {'hard':eces_hard, 'soft':eces_soft, 'credal_opt':eces_credal}
    #plot_dict_std = {'hard':std_eces_hard, 'soft':std_eces_soft, 'credal_opt':std_eces_credal}
    plot = pd.DataFrame(plot_dict)
    #wandb.log(plot_dict)
    cs = {'hard':[], 'soft':[], 'credal_opt':[]}
    sns.lineplot(data=plot).set(title="ECE")
    plt.savefig('{}/result_{}_{}_min_ece'.format(par.dataset,par.rep_per_class*num_classes,split))

    for cl in range(num_classes):
        plt.figure()
        plot_dict_class = {'hard_'+str(cl):acc_per_class_hard[:,cl], 'soft_'+str(cl):acc_per_class_soft[:,cl], 'credal_opt_'+str(cl):acc_per_class_cred[:,cl]}
        #wandb.log(plot_dict_class)
        plot = pd.DataFrame(plot_dict_class)
        sns.lineplot(data=plot).set(title="Accuracy")
        plt.savefig('{}/result_{}_class_{}_min'.format(par.dataset,par.rep_per_class*num_classes,cl))
        
compare_self_learn()

def prob2xy(p1,p2,p3):
    return p1,p2#[p3/(1-p3)*p2+(1-p3)/2,(1-p3)/2]
    

def plot_point(bounds,predictions,p_opt,i):
    tri = np.array([[0,0],[1,0],[0,1]])
    
    upper = np.max(bounds,axis=0)
    lower = np.min(bounds,axis=0)
    
    plt.figure()
    
    plt.plot(tri[:-1,0],tri[:-1,1],color='blue',label='Domain')
    plt.plot(tri[1:,0],tri[1:,1],color='blue')
    plt.plot([tri[0,0],tri[2,0]],[tri[0,1],tri[2,1]],color='blue')
    
    x_pred,y_pred = prob2xy(predictions[0],predictions[1],predictions[2])
    
    point1 = prob2xy(upper[0],lower[1],lower[2])
    point2 = prob2xy(lower[0],upper[1],lower[2])
    point3 = prob2xy(lower[0],lower[1],upper[2])
    
    p_point= prob2xy(p_opt[0],p_opt[1],p_opt[2])
    
    tri_upper = np.array([point1,point2,point3])
    
    plt.scatter(tri_upper[:, 0], tri_upper[:, 1])
    
        
    t2 = plt.Polygon(tri_upper, color='red',label='Credal Set')
    plt.gca().add_patch(t2)
    
    plt.scatter([x_pred],[y_pred],s=100,label='Prediction')
    plt.scatter([p_point[0]],[p_point[1]],s=100,label='Optimal p')
    
    plt.legend()
    plt.savefig('notebooks/point_img/img_'+str(i))
    #plt.xlim([0,1])
    #plt.ylim([0,0.6])
#plot_point(venn_pred,predictions,p_opt)

'''
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=.33,random_state=96)
split = 100
X_labeled,y_labeled = X_train[:split],y_train[:split]
rep_per_class=7
indexes = np.zeros((num_classes*rep_per_class)).astype(int)
for cl in range(num_classes):
    indexes[cl*rep_per_class:(cl+1)*rep_per_class] = np.where(y_labeled[:,cl]==1)[0][:rep_per_class]
X_cal,y_cal = X_labeled[indexes],y_labeled[indexes]
X_labeled = np.delete(X_labeled,indexes,0)
y_labeled = np.delete(y_labeled,indexes,0)
X_cal,y_cal = shuffle(X_cal,y_cal)

predictions = clf.predict_proba(X_test)
for i in range(len(X_test)):
    venn_pred = venn_prediction(X_test[i],X_cal,y_cal,clf,nn_v1)
    upper = np.max(venn_pred,axis=0)
    lower = np.min(venn_pred,axis=0)
    if all(np.logical_and(predictions[i]<upper,predictions[i]>lower)):
        p_opt=predictions[i]
    else:
        _,p_opt =solve_cvx(upper,lower,predictions[i],np)
    plot_point(venn_pred,predictions[i],p_opt,i)    
'''
