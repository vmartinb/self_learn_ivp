import numpy as np
#from keras.models import load_model
#from keras import backend as K
#import tensorflow as tf
from sklearn.neighbors import KNeighborsClassifier,NearestNeighbors
from sklearn.manifold import TSNE
from sklearn.metrics import silhouette_score
import sys
import pickle
import config
#from scipy.spatial import distance_matrix

def nonzero_mean(matrix):
    return np.mean(matrix[matrix!=0])

def distance_matrix(A,B):
    dist = lambda p1, p2: np.sqrt(((p1-p2)**2).sum())
    dm = np.asarray([[dist(p1, p2) for p2 in A] for p1 in B])
    return dm
'''
tf_config = tf.compat.v1.ConfigProto()
tf_config.gpu_options.per_process_gpu_memory_fraction = 0.3
session = tf.compat.v1.Session(config=tf_config)

K.set_learning_phase(0)
'''
sys.setrecursionlimit(40000)


from sklearn.neighbors import KNeighborsClassifier
def nn_v1(y_cal,calibration_probabilities,num_classes):
    taxon='v1'
    calibration_predictions=np.argmax(calibration_probabilities,axis=1)
    #print(np.sum(calibration_predictions==y_cal)/len(y_cal))
    category_distributions=np.zeros((num_classes,num_classes))
    for i in range(len(calibration_predictions)):
        category_distributions[calibration_predictions[i],y_cal[i]]+=1
    return category_distributions

def nn_v2(X_cal,y_cal,calibration_probabilities,num_classes):
    taxon='v2'
    calibration_predictions=np.argmax(calibration_probabilities,axis=1)
    calibration_prediction_probs=np.max(calibration_probabilities,axis=1)

    category_distributions=np.zeros((2*num_classes,num_classes))
    for i in range(len(calibration_predictions)):
        category_distributions[calibration_predictions[i]*2+(calibration_prediction_probs[i]>0.75),y_cal[i]]+=1
    return category_distributions

def nn_v3(y_cal,calibration_probabilities,num_classes):
    taxon='v3'
    calibration_predictions=np.argmax(calibration_probabilities,axis=1)
    calibration_prediction_probs=np.max(calibration_probabilities,axis=1)
    sorted_probs=np.sort(calibration_probabilities,axis=1)
    second_highest_probs=sorted_probs[:,-2]

    category_distributions=np.zeros((2*num_classes,num_classes),dtype='int')
    for i in range(len(calibration_predictions)):
        category_distributions[calibration_predictions[i]*2+(second_highest_probs[i]>0.25),y_cal[i]]+=1
    return category_distributions

def nn_v4(y_cal,calibration_probabilities,num_classes):
    taxon='v4'
    calibration_predictions=np.argmax(calibration_probabilities,axis=1)
    calibration_prediction_probs=np.max(calibration_probabilities,axis=1)
    sorted_probs=np.sort(calibration_probabilities,axis=1)
    second_highest_probs=sorted_probs[:,-2]

    category_distributions=np.zeros((2*num_classes,num_classes),dtype='int')
    for i in range(len(calibration_predictions)):
        category_distributions[calibration_predictions[i]*2+(calibration_prediction_probs[i]-second_highest_probs[i]>0.5),y_cal[i]]+=1
    return category_distributions

def knn_v1(train_embeds,y_train,calibration_embeds,y_cal,num_classes):
    taxon='knn_v1'
    neigh = KNeighborsClassifier(n_neighbors=6, metric="euclidean")
    neigh.fit(train_embeds, y_train)
    calibration_predictions=neigh.predict(calibration_embeds)

    category_distributions=np.zeros((num_classes,num_classes),dtype='int')
    for i in range(num_classes):
        for j in range(num_classes):
            category_distributions[i,j]=np.count_nonzero(y_cal[calibration_predictions==i]==j)
    return category_distributions

def knn_v2(train_embeds,y_train,calibration_embeds,y_cal,num_classes):
    taxon='knn_v2'
    neigh = KNeighborsClassifier(n_neighbors=6, metric="euclidean")
    neigh.fit(train_embeds, y_train)
    calibration_predictions=neigh.predict(calibration_embeds)
    neigh_ind=neigh.kneighbors(calibration_embeds,return_distance=False)
    neigh_labels=y_train[neigh_ind]

    num_categories=(6//2+1)*num_classes
    category_distributions=np.zeros((num_categories,num_classes),dtype='int')
    for i in range(len(calibration_embeds)):
        category_distributions[calibration_predictions[i]*(6//2+1)+np.count_nonzero(neigh_labels[i]!=calibration_predictions[i])][y_cal[i]]+=1

    return category_distributions

def nc_v1(train_embeds,y_train,calibration_embeds,y_cal,num_classes):
    taxon='nc_v1'
    embeddings_size = train_embeds.shape[1]
    centroids=np.empty((num_classes,embeddings_size))
    for i in range(num_classes):
        centroids[i]=np.mean(train_embeds[y_train==i],axis=0)
    
    category_distributions=np.zeros((num_classes,num_classes),dtype='int')
    temp_distances=np.zeros(num_classes)
    for i in range(len(calibration_embeds)):
        for j in range(num_classes):
            temp_distances[j]=np.linalg.norm(calibration_embeds[i]-centroids[j])
        category_distributions[np.argmin(temp_distances)][y_cal[i]]+=1

    return category_distributions

def nc_v2(train_embeds,y_train,calibration_embeds,y_cal,num_classes):
    taxon='nc_v2'
    embeddings_size = train_embeds.shape[1]
    centroids=np.empty((num_classes,embeddings_size))
    for i in range(num_classes):
        centroids[i]=np.mean(train_embeds[y_train==i],axis=0)

    category_distributions=np.zeros((2*num_classes,num_classes),dtype='int')
    temp_distances=np.zeros(num_classes)
    for i in range(len(calibration_embeds)):
        for j in range(num_classes):
            temp_distances[j]=np.linalg.norm(calibration_embeds[i]-centroids[j])
        pred=np.argmin(temp_distances)
        dist=np.min(temp_distances)
        category_distributions[2*pred+int(dist>0.08)][y_cal[i]]+=1

    return category_distributions

def nn2_nc2(train_embeds,y_train,x_cal,calibration_embeds,y_cal,classifier_model,num_classes):
    embeddings_size = train_embeds.shape[1]
    centroids=np.empty((num_classes,embeddings_size))
    for i in range(num_classes):
        centroids[i]=np.mean(train_embeds[y_train==i],axis=0)

    calibration_probabilities=classifier_model.predict_proba(x_cal)
    calibration_predictions=np.argmax(calibration_probabilities,axis=1)
    calibration_prediction_probs=np.max(calibration_probabilities,axis=1)

    num_nc2_categories=2*num_classes
    num_nn2_categories=2*num_classes
    category_distributions=np.zeros((num_nc2_categories*num_nn2_categories,num_classes),dtype='int')

    temp_distances=np.zeros(num_classes)
    for i in range(len(calibration_embeds)):
        nn2_c=calibration_predictions[i]*2+(calibration_prediction_probs[i]>0.75)
        for j in range(num_classes):
            temp_distances[j]=np.linalg.norm(calibration_embeds[i]-centroids[j])
        pred=np.argmin(temp_distances)
        dist=np.min(temp_distances)
        nc2_c=2*pred+int(dist>0.08)
        category_distributions[nc2_c*num_nn2_categories+nn2_c][y_cal[i]]+=1

    return category_distributions


def venn_prediction(pred_new_point,y_cal,pred_cal,taxon_func):
    #X_new = np.vstack((X_cal,new_point))
    num_classes = y_cal.shape[1]
    y_cal = np.argmax(y_cal,axis=1).reshape((-1,1))
    venn_pred = np.zeros((num_classes,num_classes))
    taxonomies_cal= taxon_func(y_cal.flatten(),pred_cal,num_classes)
    for i in range(num_classes):
        y_new = np.vstack((y_cal,np.array([i])))
        pred_new = np.vstack((pred_cal,pred_new_point))
        #pred_new = clf.predict_proba(X_new)
        #pred_new = (1-epsilon)*pred_new + epsilon/num_classes
        taxonomies= taxon_func(y_new.flatten(),pred_new,num_classes)
        diff = taxonomies - taxonomies_cal
        idx = np.where(diff==1)[0][0]
        venn_pred[i] = taxonomies[idx]/np.sum(taxonomies[idx])
    return venn_pred
