import sys
sys.path.append('/home/vitorbordini/Downloads/self_learn_ivp')
import wandb
import pandas as pd
import numpy as np
from sklearn.metrics import accuracy_score,confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_digits,load_wine,load_iris
from sklearn.preprocessing import StandardScaler,normalize
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.utils import shuffle
import matplotlib.pyplot as plt
import seaborn as sns

import torch
from torch import nn
from torch.nn import functional as F
from torch.autograd import Variable
from collections import OrderedDict
import tensorflow as tf
import cvxpy as cp

import os

from neural_net import SimpleNeuralNet
from utils import expected_calibration_error
from taxonomies_calibration import *
from optimistic_solver import solve
from datasetsml import *
from parserml import argpar

def load_mixture_dataset(n_example: int,num_classes:int):
    """Create and return a mock dataset. If we plot the data, we can see
    a sort of flower. The created dataset contains n_example X of 2 features
    and their associated label

    Args:
        n_example (int): number of examples in the wanted dataset

    Returns:
        Tuple[np.ndarray, np.ndarray]: Features matrix and associated labels vector
    """
    np.random.seed(1)
    n_representation = int(n_example/num_classes)
    n_features = 2
    features = np.zeros((n_example, n_features))
    labels = np.zeros((n_example,1), dtype='uint8')
    
    for label in range(num_classes):
        mean = 6*label*np.ones(2)
        cov = [[5.5,(-1)**label*2*label],[(-1)**label*2*label,5.5]]
        index = range(n_representation*label,n_representation*(label+1))
        features[index] = np.random.multivariate_normal(mean, cov, size=n_representation)
        labels[index] = label
    data = dict()
    data['target'] = labels
    data['data'] = features
    return data


def one_hot(vector,num_classes=3):
    vector = vector.squeeze()
    b = np.zeros((vector.size,num_classes)).astype(np.int64)
    b[np.arange(vector.size), vector] = 1
    return b

def dataset(par):
    name = par.dataset
    if name =='wine':
        data = load_wine()
        n_hidden_units = 13
        par.lr = 0.05
    elif name =='digits':
        data = load_digits()
        n_hidden_units = 10
        par.lr = 0.005
    elif name =='iris':
        data = load_iris()
        n_hidden_units = 4
        par.lr = 0.05
    elif name =='syntetic':
        data = load_mixture_dataset(1200,4)
        n_hidden_units = 2
        par.lr = 0.05
    elif name =='S2D':
        data = diseases_nlp()
        n_hidden_units = 15
        par.lr = 0.05
    elif name == 'glass':
        data = glass()
        n_hidden_units = 4
        par.lr = 0.05
    elif name == 'adult':
        data = adult()
        n_hidden_units = 10
        par.lr = 0.001
    elif name == 'ecoli':
        data = ecoli()
        n_hidden_units = 5
        par.lr = 0.001
    elif name == 'fraud':
        data = fraud()
        n_hidden_units = 10
        par.lr = 0.005
    elif name == 'stroke':
        data = stroke()
        n_hidden_units = 10
        par.lr = 0.0005
    return data,n_hidden_units

wandb.login()
par = argpar()
data,n_hidden_units = dataset(par)

run = wandb.init(
    mode="disabled",
    # Set the project where this run will be logged
    project="my-awesome-project",
    # Track hyperparameters and run metadata
    config={
        "learning_rate": par.lr,
        "dataset": par.dataset,
        "rep_per_class":par.rep_per_class,
        "labeled_instances": 20
    })

X,y = data['data'],data['target']
X = normalize(X)
num_classes = np.max(y)+1

n_input = X.shape[1]
y = one_hot(y,num_classes=num_classes).squeeze()
#X,y = shuffle(X,y)
'''
if X.shape[1]==2:
    for label in range(num_classes):
        indexes = np.where(y[:,label]==1)[0]
        plt.scatter(X[indexes,0],X[indexes,1],label='class_{}'.format(label))

        
    plt.legend()
    plt.savefig('dataset')
'''



#print(cp.installed_solvers())
def solve_cvx(upper,lower,pred,np,option='max'):
    if option=='min':
        p = cp.Variable(upper.shape)
        prob = cp.Problem(cp.Minimize(cp.sum(cp.rel_entr(p,pred))),
                    [p<=upper,p>=lower,cp.sum(p)==1])
    #['CBC', 'CVXOPT', 'ECOS', 'ECOS_BB', 'GLOP', 'GLPK', 'GLPK_MI', 'GUROBI', 'MOSEK', 'OSQP', 'PDLP', 'SCIPY', 'SCS']
        prob.solve(solver='SCS')
        return prob.value,p.value
    else:
        rel_entr_vector = np.zeros(num_classes)
        prob_matrix = np.zeros((num_classes,num_classes))
        for i in range(num_classes):
            prob_matrix[i] = lower.copy()
            prob_matrix[i,i] = upper[i]
            rel_entr_vector[i] = np.sum(prob_matrix[i]*np.log(prob_matrix[i]/pred))
        return np.max(rel_entr_vector),prob_matrix[np.argmax(rel_entr_vector)]
    
def self_learn(par,X,y,taxonomy=None,st='soft',epochs=50,iterations=30):
    '''
    Self-training algorithm
        Args:
            X(ndarray, shape (N,M)): inputs
            y(ndarray, shape (N,num_classes)): outputs (hard labels)
            taxonomy(function): Venn taxonomy in case of IVP.
            lr(float): learning rate for training.
            st(string): Strategy implemented.
            epochs(int): Number of epochs to train the model on each iteration.
            iterations(int): Number of maximum iterations.
    '''
    lr = par.lr
    split = par.split_size if X.shape[0]>200 else 20
    seeds = [2,13,25,42,57,64,78,85,96]
    num_classes = y.shape[1]
    accuracies = np.zeros((iterations,len(seeds)))
    eces = np.zeros((iterations,len(seeds)))
    accuracies_per_class = np.zeros((iterations,len(seeds),num_classes))
    rep_per_class= par.rep_per_class
    print(X.shape)
    for i,seed in enumerate(seeds):
        X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=.33,random_state=seed)
        prop_per_class = split*np.unique(np.argmax(y_train,axis=1),return_counts=True)[1]/len(y_train)
        prop_per_class = (prop_per_class/np.min(prop_per_class)).astype(int)
        model = SimpleNeuralNet(0.01,n_input,n_hidden_units,num_classes)
        indexes = []
        for cl in range(num_classes):
            indexes.append(np.where(y_train[:,cl]==1)[0][:prop_per_class[cl]])
        indexes = np.concatenate(indexes)
        X_labeled,y_labeled = X_train[indexes],y_train[indexes]
        X_unlabeled = np.delete(X_train,indexes,0)
        y_unlabeled = np.delete(y_train,indexes,0)
        X_labeled,y_labeled = shuffle(X_labeled,y_labeled)

        X_labeled,y_labeled = X_train[:split],y_train[:split]
        X_unlabeled = X_train[split:]

        prop = int(len(X_unlabeled)/iterations)
        cond =  st == 'credal_opt' or st == 'credal_bot' or st == 'credal_pes'
        if cond:
            indexes = []
            for cl in range(num_classes):
                    indexes.append(np.where(y_labeled[:,cl]==1)[0][:rep_per_class])
            indexes = np.concatenate(indexes)
            X_cal,y_cal = X_labeled[indexes],y_labeled[indexes]
            X_labeled = np.delete(X_labeled,indexes,0)
            y_labeled = np.delete(y_labeled,indexes,0)
            X_cal,y_cal = shuffle(X_cal,y_cal)

        model.fit(X_labeled,y_labeled,epochs,lr,verbose=False)
        for j in range(iterations):            
            if st =='hard':
                y_unlabeled = model.predict_proba(X_unlabeled)
                y_unlabeled = np.expand_dims(np.argmax(y_unlabeled,axis=1),axis=1)
                y_unlabeled = one_hot(y_unlabeled,num_classes).squeeze()
                y_unlabeled = y_unlabeled[j*prop:(j+1)*prop]
                y_acc = np.vstack((y_labeled,y_unlabeled))
                X_acc = np.vstack((X_labeled,X_unlabeled[j*prop:(j+1)*prop]))

            elif cond:
                y_unlabeled = model.predict_proba(X_unlabeled)
                epsilon = 0.01
                y_unlabeled = (1-epsilon)*y_unlabeled + epsilon/num_classes
                pred_cal = model.predict_proba(X_cal)
                pred_cal = (1-epsilon)*pred_cal + epsilon/num_classes
                s_matrix = np.zeros((len(X_unlabeled),num_classes))
                for k in range(len(X_unlabeled)):
                    venn_pred = venn_prediction(X_unlabeled[k],y_unlabeled[k],X_cal,y_cal,pred_cal,nn_v3)
                    upper = np.max(venn_pred,axis=0)
                    lower = np.min(venn_pred,axis=0)
                    upper = (1-epsilon)*upper + epsilon/num_classes
                    lower = (1-epsilon)*lower + epsilon/num_classes
                    if st == 'credal_opt':
                        _,p_opt = solve_cvx(upper,lower,y_unlabeled[k],np,option='min')
                    elif st == 'credal_pes':
                        _,p_opt = solve_cvx(upper,lower,y_unlabeled[k],np,option='max')
                    elif st == 'credal_bot':
                        _,p_pes = solve_cvx(upper,lower,y_unlabeled[k],np,option='max')
                        _,p_op = solve_cvx(upper,lower,y_unlabeled[k],np,option='min')
                        lamb = 0.0018*j
                        p_opt = lamb*p_op + (1-lamb)*p_pes

                    y_unlabeled[k] = p_opt
                y_acc = np.vstack((y_labeled,y_unlabeled))
                X_acc = np.vstack((X_labeled,X_unlabeled))
            elif st == 'soft':
                y_unlabeled = model.predict_proba(X_unlabeled)
                y_acc = np.vstack((y_labeled,y_unlabeled))
                X_acc = np.vstack((X_labeled,X_unlabeled))

            model.fit(X_acc,y_acc,epochs,lr,verbose=False)
            test_pred = model(torch.from_numpy(X_test).float()).detach().numpy()

            matrix = confusion_matrix(y_true=np.argmax(y_test,axis=1),
                                          y_pred=np.argmax(test_pred,axis=1))
            accuracies[j,i] = np.sum(np.diag(matrix))/np.sum(matrix)
            eces[j,i] =  expected_calibration_error(test_pred,y_test)
            accuracies_per_class[j,i,:]=(matrix.astype('float') / matrix.sum(axis=1)[:, np.newaxis]).diagonal()

    return model,accuracies,accuracies_per_class,eces


def compare_self_learn(par,X,y,iterations):
    if not os.path.exists(par.dataset):
        os.mkdir(par.dataset)
    '''
    Employs three different strategies to solve self-learning problem.
    '''
    num_classes = y.shape[1]
    model,acc_cred_opt,acc_per_class_cred_opt,eces_credal_opt = self_learn(par,X,y,st='credal_opt',iterations=iterations)
    acc_cred_opt = np.mean(acc_cred_opt,axis=1)
    eces_credal_opt = np.mean(eces_credal_opt,axis=1)
    acc_per_class_cred_opt = np.mean(acc_per_class_cred_opt,axis=1)
    print(acc_cred_opt)

    model,acc_cred_pes,acc_per_class_cred_pes,eces_credal_pes = self_learn(par,X,y,st='credal_pes',iterations=iterations)
    acc_cred_pes = np.mean(acc_cred_pes,axis=1)
    eces_credal_pes = np.mean(eces_credal_pes,axis=1)
    acc_per_class_cred_pes = np.mean(acc_per_class_cred_pes,axis=1)
    print(acc_cred_pes)


    model,acc_cred_bot,acc_per_class_cred_bot,eces_credal_bot = self_learn(par,X,y,st='credal_bot',iterations=iterations)
    acc_cred_bot = np.mean(acc_cred_bot,axis=1)
    eces_credal_bot = np.mean(eces_credal_bot,axis=1)
    acc_per_class_cred_bot = np.mean(acc_per_class_cred_bot,axis=1)
    print(acc_cred_bot)

    _,acc_hard,acc_per_class_hard,eces_hard = self_learn(par,X,y,st='hard',iterations=iterations)
    eces_hard = np.mean(eces_hard,axis=1)
    acc_hard = np.mean(acc_hard,axis=1)
    acc_per_class_hard = np.mean(acc_per_class_hard,axis=1)

    print(acc_hard)
    
    _,acc_soft,acc_per_class_soft,eces_soft = self_learn(par,X,y,st='soft',iterations=iterations)
    eces_soft = np.mean(eces_soft,axis=1)
    acc_soft = np.mean(acc_soft,axis=1)
    acc_per_class_soft = np.mean(acc_per_class_soft,axis=1)
    print(acc_soft)
    
    
    plt.figure()
    plot_dict = {'hard':acc_hard, 'soft':acc_soft, 'credal_opt':acc_cred_opt,'credal_pes':acc_cred_pes,'credal_both':acc_cred_bot}
    plot = pd.DataFrame(plot_dict)
    wandb.log(plot_dict)
    sns.lineplot(data=plot).set(title="Accuracy")
    plt.savefig('{}/result_{}_{}_all'.format(par.dataset,par.rep_per_class*num_classes,par.split_size))


    plt.figure()
    plot_dict = {'hard':eces_hard, 'soft':eces_soft, 'credal_opt':eces_credal_opt,'credal_pes':eces_credal_pes,'credal_both':eces_credal_bot}
    plot = pd.DataFrame(plot_dict)
    wandb.log(plot_dict)
    sns.lineplot(data=plot).set(title="ECE")
    plt.savefig('{}/result_{}_{}_all_ece'.format(par.dataset,par.rep_per_class*num_classes,par.split_size))

    for cl in range(num_classes):
        plt.figure()
        plot_dict_class = {'hard_'+str(cl):acc_per_class_hard[:,cl], 'soft_'+str(cl):acc_per_class_soft[:,cl], 
                           'credal_opt_'+str(cl):acc_per_class_cred_opt[:,cl],
                           'credal_pes_'+str(cl):acc_per_class_cred_pes[:,cl],
                           'credal_both_'+str(cl):acc_per_class_cred_bot[:,cl]}
        wandb.log(plot_dict_class)
        plot = pd.DataFrame(plot_dict_class)
        sns.lineplot(data=plot).set(title="Accuracy")
        plt.savefig('{}/result_{}_class_{}_all'.format(par.dataset,par.rep_per_class*num_classes,cl))

    return model

clf = compare_self_learn(par,X,y,iterations=50)

def prob2xy(p1,p2,p3):
    return p1,p2#[p3/(1-p3)*p2+(1-p3)/2,(1-p3)/2]
    

def plot_point(bounds,predictions,p_opt,i):
    tri = np.array([[0,0],[1,0],[0,1]])
    
    upper = np.max(bounds,axis=0)
    lower = np.min(bounds,axis=0)
    
    plt.figure()
    
    plt.plot(tri[:-1,0],tri[:-1,1],color='blue',label='Domain')
    plt.plot(tri[1:,0],tri[1:,1],color='blue')
    plt.plot([tri[0,0],tri[2,0]],[tri[0,1],tri[2,1]],color='blue')
    
    x_pred,y_pred = prob2xy(predictions[0],predictions[1],predictions[2])
    
    point1 = prob2xy(upper[0],lower[1],lower[2])
    point2 = prob2xy(lower[0],upper[1],lower[2])
    point3 = prob2xy(lower[0],lower[1],upper[2])
    
    p_point= prob2xy(p_opt[0],p_opt[1],p_opt[2])
    
    tri_upper = np.array([point1,point2,point3])
    
    plt.scatter(tri_upper[:, 0], tri_upper[:, 1])
    
        
    t2 = plt.Polygon(tri_upper, color='red',label='Credal Set')
    plt.gca().add_patch(t2)
    
    plt.scatter([x_pred],[y_pred],s=100,label='Prediction')
    plt.scatter([p_point[0]],[p_point[1]],s=100,label='Optimal p')
    
    plt.legend()
    plt.savefig('notebooks/point_img/img_'+str(i))
    #plt.xlim([0,1])
    #plt.ylim([0,0.6])
#plot_point(venn_pred,predictions,p_opt)

'''
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=.33,random_state=96)
split = 100
X_labeled,y_labeled = X_train[:split],y_train[:split]
rep_per_class=7
indexes = np.zeros((num_classes*rep_per_class)).astype(int)
for cl in range(num_classes):
    indexes[cl*rep_per_class:(cl+1)*rep_per_class] = np.where(y_labeled[:,cl]==1)[0][:rep_per_class]
X_cal,y_cal = X_labeled[indexes],y_labeled[indexes]
X_labeled = np.delete(X_labeled,indexes,0)
y_labeled = np.delete(y_labeled,indexes,0)
X_cal,y_cal = shuffle(X_cal,y_cal)

predictions = clf.predict_proba(X_test)
for i in range(len(X_test)):
    venn_pred = venn_prediction(X_test[i],X_cal,y_cal,clf,nn_v1)
    upper = np.max(venn_pred,axis=0)
    lower = np.min(venn_pred,axis=0)
    if all(np.logical_and(predictions[i]<upper,predictions[i]>lower)):
        p_opt=predictions[i]
    else:
        _,p_opt =solve_cvx(upper,lower,predictions[i],np)
    plot_point(venn_pred,predictions[i],p_opt,i)    
'''