"""This file contains the Inductive Venn and Venn Abers and algorithm.

Self Learning using Venn Abers predictors

@Côme Rodriguez, @Vitor Bordini, @Sébastien Destercke and @Benjamin Quost
"""

from typing import Union, List
import numpy as np
import pandas as pd
import torch
from sklearn.isotonic import IsotonicRegression
from config import Config
import pickle

def venn_abers_pytorch(
    calibrations: torch.Tensor,
    calibration_labels: pd.Series,
    predictions: torch.Tensor
)-> List:
    """Compute the Inductive Venn Abers algorithm to obtain a credal set of probabilities
    for a test instance

    Args:
        trained_classifier (Union[SimpleNeuralNet, SimpleNeuralNetCredal]): A binary classifier
            train on some data and having a 'predict_probas' method. Here, implementation only
            for one of the classifier in 'neural_net.py'.
        calibration_features (torch.Tensor): calibration features set
        calibration_labels (pd.Series): corresponding labels for calibration features
        test_instance (torch.Tensor): new features without a corresponding label

    Returns:
        List: credal set [p0, p1]
    """
    interval = []
    aux = calibrations[:,0].reshape((-1,1))
    scores = np.vstack((aux,predictions[0]))
    for i in [[0, 1],[1,0]]:
        classy = i
        y1 = np.vstack((calibration_labels,classy))
        y1 = np.argmax(y1,axis=1)
        g = IsotonicRegression(y_min=0.001, y_max=0.999, out_of_bounds='clip')
        g.fit(scores, y1.flatten())
        interval.append(g.predict(scores[-1])[0])
    interval = np.array(interval)
    return interval