"""This file contains usefull functions

Self Learning using Venn Abers predictors

@Côme Rodriguez, @Vitor Bordini, @Sébastien Destercke and @Benjamin Quost
"""

from typing import List, Tuple
import pandas as pd
from scipy.special import rel_entr
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import torch
from torch.autograd import Variable
import torch.nn.functional as F
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import confusion_matrix
from datasetsml import *
from sklearn.datasets import load_digits,load_wine,load_iris
import cvxpy as cp
from neural_net import ConvNet

def one_hot(vector,num_classes=3):
    vector = vector.squeeze()
    b = np.zeros((vector.size,num_classes)).astype(np.int64)
    b[np.arange(vector.size), vector] = 1
    return b

def KLD_loss_credal(y_true: torch.Tensor, y_pred: torch.Tensor) -> torch.Tensor:
    """Compute the KLD Loss D_KL(y_true || y_pred) for binary classification
    with credal sets as Y_train

    Args:
        y_true (torch.Tensor): Y_train in form of credal sets 
        y_pred (torch.Tensor): probabilities output by a binary classifier

    Returns:
        torch.Tensor: KLD Loss (with requires_grad=True for gradient descent)
    """    
    y_true_inf = y_true[:, 0].reshape(-1, 1)
    y_true_sup = y_true[:, 1].reshape(-1, 1)
    used_probas = torch.zeros(y_true.shape[0], 1)
    used_probas[y_pred <= y_true_inf] = y_true_inf[y_pred <= y_true_inf]
    used_probas[y_pred >= y_true_sup] = y_true_sup[y_pred >= y_true_sup]
    used_probas[(y_pred >= y_true_inf) & (y_pred <= y_true_sup)
                ] = y_pred[(y_pred >= y_true_inf) & (y_pred <= y_true_sup)]
    #loss = torch.mean(used_probas* torch.log(used_probas/y_pred) + (1-used_probas)*torch.log((1-used_probas)/(1-y_pred)))
    loss = F.kl_div(used_probas,y_pred,reduction='mean')
    return loss



def get_right_probability_distribution(
    probability_interval: List[float],
    estimated_probability: float
) -> float:
    """Get the right probability distribution to use in the loss function
    when dealing with intervals of probabilities labels. The distribution
    (i.e the probability p(Y=1|X=x)) to use is determined with the following:
    If estimated_probability < inf(probability_interval):
        return inf(probability_interval)
    Else if estimated_probability > sup(probability_interval):
        return sup(probability_interval)
    Else return estimated_probability

    Args:
        probability_interval (List[float]): interval of probabilities (representing
            the uncertainty about the label) we want to learn from
        estimated_probability (float): probability p(Y=1|X=x) estimated by a model

    Returns:
        (float): probability to use in the loss function
    """
    if estimated_probability < probability_interval[0]:
        return probability_interval[0]
    if estimated_probability > probability_interval[1]:
        return probability_interval[1]
    return estimated_probability

def kullback_leibler_divergence(probability: float, estimated_probability: float) -> float:
    """Compute the Kullback Leibler divergence between to distributions
    of probabilities. The formula is the following:
        D_kl(p||q) = p * ln(p/q) + (1-p) * ln((1-p)/(1-q))

    Args:
        probability (float): observed probability
        estimated_probability (float): probability estimated by a model

    Returns:
        (float): Kullback Leibler divergence between probability and
            estimated_probability
    """
    return rel_entr(probability, estimated_probability)\
        + rel_entr(1-probability, 1-estimated_probability)

def get_accuracy(y_true: pd.Series, y_pred: pd.Series) -> float:
    """Compute the accuracy score between y_true and y_pred

    Args:
        y_true (pd.Series): true labels
        y_pred (pd.Series): predicted labels

    Returns:
        (float): accuracy score
    """
    accuracy = len(
        y_pred[
            y_pred==y_true
        ]
    )/len(y_pred)
    return accuracy

def plot_decision_boundary(dataset, labels, model, steps=1000, color_map='Paired'):
    color_map = plt.get_cmap(color_map)
    # Define region of interest by data limits
    print()
    xmin, xmax = dataset.to_numpy()[:, 0].min() - 1, dataset.to_numpy()[:, 0].max() + 1
    ymin, ymax = dataset.to_numpy()[:, 1].min() - 1, dataset.to_numpy()[:, 1].max() + 1
    steps = 1000
    x_span = np.linspace(xmin, xmax, steps)
    y_span = np.linspace(ymin, ymax, steps)
    xx, yy = np.meshgrid(x_span, y_span)

    # Make predictions across region of interest
    model.eval()
    labels_predicted = model(Variable(torch.from_numpy(np.c_[xx.ravel(), yy.ravel()]).float()))

    # Plot decision boundary in region of interest
    labels_predicted = [0 if value <= 0.5 else 1 for value in labels_predicted.detach().numpy()]
    z = np.array(labels_predicted).reshape(xx.shape)
    
    fig, ax = plt.subplots(figsize=(20, 10))
    ax.contourf(xx, yy, z, cmap=color_map, alpha=0.2)
    
    return fig, ax

def cartesian_to_barycentric(x, y, x1, y1, x2, y2, x3, y3):
    # Defina os vértices de referência do triângulo no espaço 3D
    vertex1 = np.array([0.0, 0.0, 1.0])
    vertex2 = np.array([1.0, 0.0, 0.0])
    vertex3 = np.array([0.0, 1.0, 0.0])

    # Ponto que você deseja projetar
    point = np.array([x,y, 1-x-y])

    # Calcule as coordenadas baricêntricas
    v0 = vertex2 - vertex1
    v1 = vertex3 - vertex1
    v2 = point - vertex1

    dot00 = np.dot(v0, v0)
    dot01 = np.dot(v0, v1)
    dot02 = np.dot(v0, v2)
    dot11 = np.dot(v1, v1)
    dot12 = np.dot(v1, v2)

    # Calcule os pesos das coordenadas baricêntricas
    denom = dot00 * dot11 - dot01 * dot01
    u = (dot11 * dot02 - dot01 * dot12) / denom
    v = (dot00 * dot12 - dot01 * dot02) / denom
    w = 1.0 - u - v# Defina os vértices de referência do triângulo no espaço 3D

    projected_point = u * vertex1 + v * vertex2 + w * vertex3

    print("Coordenadas Baricêntricas: u =", u, "v =", v, "w =", w)
    print("Posição projetada:", projected_point)
    return projected_point[:2]

def prob2xy(p1,p2,p3):
    x1,y1 = 0,0
    x2,y2 = 0,1
    x3,y3 = 1,0
     
    return p1,p2#cartesian_to_barycentric(p1,p2,x1, y1, x2, y2, x3, y3)
    

def plot_point(pat,bounds,predictions,p_opt,i):
    tri = np.array([[0,0],[1,0],[0,1]])
    
    upper = np.max(bounds,axis=0)
    lower = np.min(bounds,axis=0)
    
    plt.figure()
    
    plt.plot(tri[:-1,0],tri[:-1,1],color='blue',label='Domain')
    plt.plot(tri[1:,0],tri[1:,1],color='blue')
    plt.plot([tri[0,0],tri[2,0]],[tri[0,1],tri[2,1]],color='blue')
    
    x_pred,y_pred = prob2xy(predictions[0],predictions[1],predictions[2])
    point1 = prob2xy(upper[0],lower[1],lower[2])
    point2 = prob2xy(lower[0],upper[1],lower[2])
    point3 = prob2xy(lower[0],lower[1],upper[2])
    
    p_point= prob2xy(p_opt[0],p_opt[1],p_opt[2])
    
    tri_upper = np.array([point1,point2,point3])
    
    plt.scatter(tri_upper[:, 0], tri_upper[:, 1])

    t2 = plt.Polygon(tri_upper, color='red',label='Credal Set')
    plt.gca().add_patch(t2)
    
    plt.scatter([x_pred],[y_pred],s=100,label='Prediction')
    plt.scatter([p_point[0]],[p_point[1]],s=100,label='Optimal p')
    
    plt.legend()
    plt.savefig(pat+'im/im_'+str(i))
    plt.close()


def expected_calibration_error(samples, true_labels,num_classes, M=3):
    # uniform binning approach with M number of bins
    bin_boundaries = np.linspace(0, 1, M + 1)
    bin_lowers = bin_boundaries[:-1]
    bin_uppers = bin_boundaries[1:]

    true_labels = np.argmax(true_labels,axis=1)
    if num_classes==2:
        # keep confidences / predicted "probabilities" as they are
        confidences = samples
        # get binary predictions from confidences
        predicted_label = (samples>0.5).astype(float)

        accuracies = predicted_label==true_labels.reshape(-1,1)
    else:                                          
        # get max probability per sample i                 
        confidences = np.max(samples, axis=1)               
        # get predictions from confidences (positional in this case)
        predicted_label = np.argmax(samples, axis=1).astype(float)

        accuracies = predicted_label==true_labels

    ece = np.zeros(1)
    for bin_lower, bin_upper in zip(bin_lowers, bin_uppers):
        # determine if sample is in bin m (between bin lower & upper)
        in_bin = np.logical_and(confidences > bin_lower.item(), confidences <= bin_upper.item())
        # can calculate the empirical probability of a sample falling into bin m: (|Bm|/n)
        prop_in_bin = in_bin.astype(float).mean()

        if prop_in_bin.item() > 0:
            # get the accuracy of bin m: acc(Bm)
            accuracy_in_bin = accuracies[in_bin].astype(float).mean()
            # get the average confidence of bin m: conf(Bm)
            avg_confidence_in_bin = confidences[in_bin].mean()
            # calculate |acc(Bm) - conf(Bm)| * (|Bm|/n) for bin m and add to the total ECE
            ece += np.abs(avg_confidence_in_bin - accuracy_in_bin) * prop_in_bin
    return ece


def solve_cvx(param):
    upper=param['upper']
    lower=param['lower']
    option=param['option']
    pred=param['pred']
    if np.all(np.logical_or(pred<lower, pred > upper)):
        p = cp.Variable(upper.shape)
        prob = cp.Problem(cp.Minimize(cp.sum(cp.rel_entr(p,pred))),
                [p<=upper,p>=lower,cp.sum(p)==1])
#['CBC', 'CVXOPT', 'ECOS', 'ECOS_BB', 'GLOP', 'GLPK', 'GLPK_MI', 'GUROBI', 'MOSEK', 'OSQP', 'PDLP', 'SCIPY', 'SCS']
        prob.solve(solver='SCS')
        return p.value
    else:
        return pred

def asymmetric_noise_for_dataset(par,X_test,y_test, label_noise, num_classes, X_train,training_targets,seed):
    CIFAR10_STR = "cifar10"
    CIFAR100_STR = "cifar100"
    #training_targets = training_targets.numpy()

    # Set NumPy random seed
    np.random.seed(seed)
    if par.im_data:
        device = 'cuda:'+ par.cuda if par.cuda!=-1 else 'cpu'
        noise_model = ConvNet(0.01,device,num_classes)
        noise_model.fit(x_train=X_train,y_train=one_hot(training_targets,num_classes),epochs=20,learning_rate=0.0001)
        y_pred = np.argmax(noise_model.predict_proba(X_test,as_numpy=True,from_numpy=True),axis=1)
    else:
        noise_model = GaussianNB().fit(X_train,training_targets)
        y_pred = noise_model.predict(X_test)
    matrix = confusion_matrix(y_test,y_pred,normalize='true')
    num_classes = len(matrix)
    pd.DataFrame(matrix).round(2).to_csv(
        'noisy/{}/result_{}_confusion_matrix.csv'.format(par.dataset,par.rep_per_class*num_classes))
    aux = np.zeros_like(training_targets)
    for i in range(num_classes):
        idx = training_targets==i
        aux[idx] = np.random.choice(num_classes,size=len(aux[idx]),p=matrix[i])
    training_targets = aux.copy()
    return training_targets

def symmetric_noise(par,X,y,p,num_classes,X_train,labels,seed):
    np.random.seed(seed)
    vector = np.random.uniform(size=len(labels))
    mask = vector<p
    labels[mask]=np.random.uniform(low=0,high=num_classes,size=len(labels[mask]))
    return labels


def dataset(par):
    name = par.dataset
    if name =='wine':
        data = load_wine()
        n_hidden_units = 5
        par.lr = 0.03
        par.rep_per_class = 2
        par.split_size=0.1
    elif name =='digits':
        data = load_digits()
        n_hidden_units = 10
        par.lr = 0.0001
    elif name =='iris':
        data = load_iris()
        n_hidden_units = 4
        par.lr = 0.01
        par.rep_per_class = 2
        par.split_size=20/150
    elif name =='syntetic':
        data = load_mixture_dataset(500,3)
        n_hidden_units = 2
        par.lr = 0.00005
    elif name =='S2D':
        data = diseases_nlp()
        n_hidden_units = 15
        par.lr = 0.05
    elif name == 'glass':
        data = glass()
        n_hidden_units = 4
        par.lr = 0.005
    elif name == 'adult':
        data = adult()
        n_hidden_units = 10
        par.lr = 0.0001
    elif name == 'ecoli':
        data = ecoli()
        n_hidden_units = 5
        par.lr = 0.001
        par.split_size = 0.28
    elif name == 'australian':
        data = australian()
        n_hidden_units = 4
        par.lr = 0.005
    elif name == 'stroke':
        data = stroke()
        n_hidden_units = 6
        par.lr = 0.005
    elif name == 'beans':
        data = dry_bean()
        n_hidden_units = 5
        par.lr = 0.0005
        par.split_size=0.1
        
    elif name == 'fruits':
        data = date_fruit()
        n_hidden_units = 15
        par.lr = 0.00005
    elif name == 'heart':
        data = get_disease()
        n_hidden_units = 5
        par.lr = 0.005
    elif name == 'titanic':
        data = get_titanic()
        n_hidden_units = 4
        par.lr = 0.001
    elif name == 'letter_reco':
        data = letter_reco()
        n_hidden_units = 20
        par.lr = 0.005
    elif name == 'steel_plates':
        data = steel_plates()
        n_hidden_units = 5
        par.lr = 0.001
    elif name == 'poem':
        data = poem()
        n_hidden_units = 9
        par.lr =  0.0005
    elif name == 'wall':
        data = wall()
        n_hidden_units = 10
        par.lr = 0.00005
        par.split_size=0.15
    elif name == 'heloc':
        data = heloc()
        n_hidden_units = 5
        par.lr = 0.00001
    elif name == 'mnist':
        data = mnist()
        n_hidden_units = 5
        par.lr = 0.000001
        par.im_data = True

    elif name == 'cifar10':
        data = cifar10()
        n_hidden_units = 5
        par.lr = 0.00001
        par.im_data = True

    return data,n_hidden_units
