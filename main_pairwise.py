import sys
sys.path.append('/home/vitorbordini/Downloads/self_learn_ivp')
import wandb
import pandas as pd
import numpy as np
from sklearn.metrics import accuracy_score,confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_digits,load_wine,load_iris
from sklearn.preprocessing import StandardScaler,normalize
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.utils import shuffle
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.ensemble import IsolationForest
from tqdm import tqdm
np.seterr(divide='raise')

import torch
from torch import nn
from torch.nn import functional as F
from torch.autograd import Variable
from collections import OrderedDict
import tensorflow as tf
import cvxpy as cp

import os

from neural_net import SimpleNeuralNet,PairwiseNeuralNet
from utils import plot_decision_boundary,plot_point,dataset
from taxonomies_calibration import *
from optimistic_solver import solve
from datasetsml import *
from parserml import argpar

def discounting(pairwise_bounds,num_classes):
    epsilons = cp.Variable(num_classes*(num_classes-1)//2)
    ct_list = []
    count=0
    probs = np.zeros(num_classes)
    for i in range(num_classes-1):
        for j in range(num_classes):
            if j>i:
                epsilon = epsilons[count]
                alpha = pairwise_bounds[i,j,1] #lower bound
                beta = pairwise_bounds[i,j,0] #upper bound
                ct_list.append(epsilon*(-alpha)<=(1-alpha)*probs[i] - alpha*probs[j])
                ct_list.append(epsilon*(beta-1)<=(beta-1)*probs[i] + beta*probs[j])
                count+=1
    ct_list.append(epsilons>=0)
    ct_list.append(epsilons<=1)
    
    prob = cp.Problem(cp.Minimize(cp.sum(epsilons)),
                ct_list)
    prob.solve(solver='SCS')
    return  epsilons.value

def prob_est(pairwise_bounds,probabilities_distribution,num_classes):

    epsilons = discounting(pairwise_bounds,num_classes)
    probs = np.zeros(num_classes)
    for k in range(num_classes):
        p = cp.Variable(num_classes)
        ct_list = []
        count=0
        for i in range(num_classes-1):
            for j in range(num_classes):
                if j>i:
                    epsilon = epsilons[count]
                    count+=1
                    alpha = pairwise_bounds[i,j,1]
                    beta = pairwise_bounds[i,j,0]
                    ct_list.append(epsilon*(-alpha)<=(1-alpha)*p[i] - alpha*p[j])
                    ct_list.append(epsilon*(beta-1)<=(beta-1)*p[i] + beta*p[j])
        ct_list.append(cp.sum(p)==1)
        ct_list.append(p<=1)
        ct_list.append(p>=0)


        prob = cp.Problem(cp.Minimize(p[k]),
                        ct_list)
        prob.solve(solver='SCS')
        if p.value is not None:
            probs[k] = p.value[k]
        else:
            probs[k] = probabilities_distribution[k]
    return probs/probs.sum()
    
def load_mixture_dataset(n_example: int,num_classes:int):
    """Create and return a mock dataset. If we plot the data, we can see
    a sort of flower. The created dataset contains n_example X of 2 features
    and their associated label

    Args:
        n_example (int): number of examples in the wanted dataset

    Returns:
        Tuple[np.ndarray, np.ndarray]: Features matrix and associated labels vector
    """
    np.random.seed(1)
    n_representation = int(n_example/num_classes)
    n_features = 2
    features = np.zeros((n_example, n_features))
    labels = np.zeros((n_example,1), dtype='uint8')
    
    for label in range(num_classes):
        mean = 6*label*np.ones(2)
        cov = [[5.5,(-1)**label*2*label],[(-1)**label*2*label,5.5]]
        index = range(n_representation*label,n_representation*(label+1))
        features[index] = np.random.multivariate_normal(mean, cov, size=n_representation)
        labels[index] = label
    data = dict()
    data['target'] = labels
    data['data'] = features
    return data


def one_hot(vector,num_classes=3):
    vector = vector.squeeze()
    b = np.zeros((vector.size,num_classes)).astype(np.int64)
    b[np.arange(vector.size), vector] = 1
    return b


wandb.login()
par = argpar()
data,n_hidden_units = dataset(par)

run = wandb.init(
    mode='disabled',
    # Set the project where this run will be logged
    project="my-awesome-project",
    # Track hyperparameters and run metadata
    config={
        "learning_rate": par.lr,
        "dataset": par.dataset,
        "rep_per_class":par.rep_per_class,
        "labeled_instances": 20
    })

X,y = data['data'],data['target']
X = normalize(X)
num_classes = np.max(y)+1

n_instances,n_input = X.shape
y = one_hot(y,num_classes=num_classes).squeeze()
#X,y = shuffle(X,y)
instances_per_class =(((num_classes - np.arange(num_classes))/num_classes) * (n_instances/num_classes)).astype(int)
instances_per_class[0]-= 1
#print(instances_per_class)
indexes = []
for cl in range(num_classes):
    indexes.append(np.where(y[:,cl]==1)[0][:instances_per_class[cl]])
indexes = np.concatenate(indexes)
#X,y = X[indexes],y[indexes]
'''
if X.shape[1]==2:
    for label in range(num_classes):
        indexes = np.where(y[:,label]==1)[0]
        plt.scatter(X[indexes,0],X[indexes,1],label='class_{}'.format(label))

        
    plt.legend()
    plt.savefig('dataset')
'''



#print(cp.installed_solvers())
def solve_cvx(upper,lower,pred,np,option='max'):
    if option=='min':
        p = cp.Variable(upper.shape)
        prob = cp.Problem(cp.Minimize(cp.sum(cp.rel_entr(p,pred))),
                    [p<=upper,p>=lower,cp.sum(p)==1])
    #['CBC', 'CVXOPT', 'ECOS', 'ECOS_BB', 'GLOP', 'GLPK', 'GLPK_MI', 'GUROBI', 'MOSEK', 'OSQP', 'PDLP', 'SCIPY', 'SCS']
        prob.solve(solver='SCS')
        return prob.value,p.value
    else:
        rel_entr_vector = np.zeros(num_classes)
        prob_matrix = np.zeros((num_classes,num_classes))
        for i in range(num_classes):
            prob_matrix[i] = lower.copy()
            prob_matrix[i,i] = upper[i]
            rel_entr_vector[i] = np.sum(prob_matrix[i]*np.log(prob_matrix[i]/pred))
        return np.max(rel_entr_vector),prob_matrix[np.argmax(rel_entr_vector)]
    
def self_learn(par,X,y,taxonomy=None,st='soft',epochs=50,iterations=50):
    '''
    Self-training algorithm
        Args:
            X(ndarray, shape (N,M)): inputs
            y(ndarray, shape (N,num_classes)): outputs (hard labels)
            taxonomy(function): Venn taxonomy in case of IVP.
            lr(float): learning rate for training.
            st(string): Strategy implemented.
            epochs(int): Number of epochs to train the model on each iteration.
            iterations(int): Number of maximum iterations.
    '''
    lr = par.lr
    split = par.split_size if X.shape[0]>200 else 20
    num=1
    seeds = np.linspace(start=1,stop=100,num=num).astype(int)
    num_classes = y.shape[1]
    accuracies = np.zeros((iterations,len(seeds)))
    accuracies_per_class = np.zeros((iterations,len(seeds),num_classes))
    rep_per_class= par.rep_per_class
    loss_value_test = np.array([])
    loss_value_train = np.array([])
    print(X.shape)
    for i,seed in enumerate(seeds):
        X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=.33,random_state=seed)
        prop_per_class = np.unique(np.argmax(y,axis=1),return_counts=True)[1]/len(y)
        prop_per_class = np.round((prop_per_class/np.min(prop_per_class))).astype(int)
        model = PairwiseNeuralNet(0.01,n_input,n_hidden_units,num_classes)
        sum_prop = np.sum(prop_per_class)
        indexes = []
        for cl in range(num_classes):
            indexes.append(np.where(y_train[:,cl]==1)[0][:prop_per_class[cl]*split//sum_prop])
        indexes = np.concatenate(indexes)
        X_labeled,y_labeled = X_train[indexes],y_train[indexes]
        X_unlabeled = np.delete(X_train,indexes,0)
        y_unlabeled = np.delete(y_train,indexes,0)
        X_labeled,y_labeled = shuffle(X_labeled,y_labeled)

        #aux = X_unlabeled[y==num_classes-1]
        #point_to_plot = aux[0]  

        prop = int(len(X_unlabeled)/iterations)
        cond = st == 'credal'
        if cond:
            indexes = []
            for cl in range(num_classes):
                    indexes.append(np.where(y_labeled[:,cl]==1)[0][:rep_per_class])
            indexes = np.concatenate(indexes)
            X_cal,y_cal = X_labeled[indexes],y_labeled[indexes]
            X_labeled = np.delete(X_labeled,indexes,0)
            y_labeled = np.delete(y_labeled,indexes,0)
            X_cal,y_cal = shuffle(X_cal,y_cal)
        model.fit(X_labeled,y_labeled,epochs,lr,verbose=False)
        for j in tqdm(range(iterations)):
            if st =='hard':
                y_unlabeled = model.predict_proba(X_unlabeled,from_numpy=True,as_numpy=True)
                y_unlabeled = np.expand_dims(np.argmax(y_unlabeled,axis=1),axis=1)
                y_unlabeled = one_hot(y_unlabeled,num_classes).squeeze()
                y_unlabeled = y_unlabeled[j*prop:(j+1)*prop]
                y_acc = np.vstack((y_labeled,y_unlabeled))
                X_acc = np.vstack((X_labeled,X_unlabeled[j*prop:(j+1)*prop]))

            elif cond:
                y_unlabeled = model.predict_proba(X_unlabeled,from_numpy=True,as_numpy=True)
                epsilon = 0.01
                y_unlabeled = (1-epsilon)*y_unlabeled + epsilon/num_classes
                pred_cal = model.predict_proba(X_cal,from_numpy=True,as_numpy=True)
                pred_cal = (1-epsilon)*pred_cal + epsilon/num_classes
                for k in range(len(X_unlabeled)):
                    pairwise_bounds = model.multi_class_IVAP(X_cal,y_cal,X_unlabeled[k])            
                    p_opt = prob_est(pairwise_bounds,y_unlabeled[k],num_classes=num_classes)
                    y_unlabeled[k] = p_opt
                
                y_acc = np.vstack((y_labeled,y_unlabeled))
                X_acc = np.vstack((X_labeled,X_unlabeled))

            elif st == 'soft':
                y_unlabeled = model.predict_proba(X_unlabeled,from_numpy=True,as_numpy=True)
                y_acc = np.vstack((y_labeled,y_unlabeled))
                X_acc = np.vstack((X_labeled,X_unlabeled))
            model.fit(X_acc,y_acc,epochs,lr,verbose=False)

            test_pred = model.predict_proba(X_test,from_numpy=True)
            y_test_nonzero = y_test.copy().astype(np.float64)
            y_test_nonzero[y_test_nonzero==0]+= 0.001
            y_test_nonzero[y_test_nonzero==1]-= 0.001
            loss_value_test = np.hstack((loss_value_test,(y_test_nonzero*(np.log(y_test_nonzero)-np.log(test_pred))).sum(axis=1).mean()))
            plt.plot(loss_value_test)
            plt.savefig('pairwise/{}/loss_test_{}_min'.format(par.dataset,st))
            plt.clf()
            
            y_train_nonzero = y_train.copy().astype(np.float64)
            y_train_nonzero[y_train_nonzero==0]+= 0.001
            y_train_nonzero[y_train_nonzero==1]-= 0.001
            train_pred = model.predict_proba(X_train,from_numpy=True)
            loss_value_train = np.hstack((loss_value_train,(y_train_nonzero*(np.log(y_train_nonzero)-np.log(train_pred))).sum(axis=1).mean()))
            plt.plot(loss_value_train)
            plt.savefig('pairwise/{}/loss_train_{}_min'.format(par.dataset,st))
            plt.clf()       

            torch.save(model.state_dict(),'{}/model_{}_{}_{}_{}_min.pth'.format(par.dataset,par.rep_per_class*num_classes,par.split_size,j,st))
            matrix = confusion_matrix(y_true=np.argmax(y_test,axis=1),
                                          y_pred=np.argmax(test_pred,axis=1))
            accuracies[j,i] = np.sum(np.diag(matrix))/np.sum(matrix)
            accuracies_per_class[j,i,:]=(matrix.astype('float') / matrix.sum(axis=1)[:, np.newaxis]).diagonal()

    return model,accuracies,accuracies_per_class

def compare_self_learn(par,X,y,iterations):
    if not os.path.exists('pairwise'):
        os.mkdir('pairwise')
    if not os.path.exists('pairwise/' + par.dataset):
        os.mkdir('pairwise/' + par.dataset)
    '''
    Employs three different strategies to solve self-learning problem.
    '''
    num_classes = y.shape[1]

    _,acc_soft,acc_per_class_soft = self_learn(par,X,y,st='soft',iterations=iterations)
    acc_soft = np.mean(acc_soft,axis=1)
    acc_per_class_soft = np.mean(acc_per_class_soft,axis=1)
    print(acc_soft)

    model,acc_cred_opt,acc_per_class_cred_opt = self_learn(par,X,y,st='credal',iterations=iterations)
    acc_cred_opt = np.mean(acc_cred_opt,axis=1)
    acc_per_class_cred_opt = np.mean(acc_per_class_cred_opt,axis=1)
    print(acc_cred_opt)

    _,acc_hard,acc_per_class_hard = self_learn(par,X,y,st='hard',iterations=iterations)
    acc_hard = np.mean(acc_hard,axis=1)
    acc_per_class_hard = np.mean(acc_per_class_hard,axis=1)
    print(acc_hard)
    
    plt.figure()
    plot_dict = {'hard':acc_hard, 'soft':acc_soft, 'credal':acc_cred_opt}
    plot = pd.DataFrame(plot_dict)
    wandb.log(plot_dict)
    sns.lineplot(data=plot).set(title="Accuracy")
    plt.savefig('pairwise/{}/result_{}_{}_min'.format(par.dataset,par.rep_per_class*num_classes,par.split_size))

    for cl in range(num_classes):
        plt.figure()
        plot_dict_class = {'hard_'+str(cl):acc_per_class_hard[:,cl], 'soft_'+str(cl):acc_per_class_soft[:,cl], 'credal'+str(cl):acc_per_class_cred_opt[:,cl],
                           }
        wandb.log(plot_dict_class)
        plot = pd.DataFrame(plot_dict_class)
        sns.lineplot(data=plot).set(title="Accuracy")
        plt.savefig('pairwise/{}/result_{}_class_{}_min'.format(par.dataset,par.rep_per_class*num_classes,cl))

    return model

clf = compare_self_learn(par,X,y,iterations=50)

    #plt.xlim([0,1])
    #plt.ylim([0,0.6])
#plot_point(venn_pred,predictions,p_opt)

'''
X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=.33,random_state=96)
split = 100
X_labeled,y_labeled = X_train[:split],y_train[:split]
rep_per_class=7
indexes = np.zeros((num_classes*rep_per_class)).astype(int)
for cl in range(num_classes):
    indexes[cl*rep_per_class:(cl+1)*rep_per_class] = np.where(y_labeled[:,cl]==1)[0][:rep_per_class]
X_cal,y_cal = X_labeled[indexes],y_labeled[indexes]
X_labeled = np.delete(X_labeled,indexes,0)
y_labeled = np.delete(y_labeled,indexes,0)
X_cal,y_cal = shuffle(X_cal,y_cal)

predictions = clf.predict_proba(X_test)
for i in range(len(X_test)):
    venn_pred = venn_prediction(X_test[i],X_cal,y_cal,clf,nn_v1)
    upper = np.max(venn_pred,axis=0)
    lower = np.min(venn_pred,axis=0)
    if all(np.logical_and(predictions[i]<upper,predictions[i]>lower)):
        p_opt=predictions[i]
    else:
        _,p_opt =solve_cvx(upper,lower,predictions[i],np)
    plot_point(venn_pred,predictions[i],p_opt,i)    
'''