from sklearn.model_selection import train_test_split
import numpy as np
from sklearn.preprocessing import LabelEncoder,MinMaxScaler, StandardScaler
import pandas as pd
from sentence_transformers import SentenceTransformer
import os
from tensorflow.keras.preprocessing.image import img_to_array,load_img
from tensorflow.keras.utils import to_categorical
from sklearn.impute import KNNImputer

def fraud():
	data = dict()
	df = pd.read_csv("data/Base.csv")
	y = df["fraud_bool"]
	X = df.drop(["fraud_bool", "device_os", "device_fraud_count", "month"], axis = 1)
	data['data'] = X
	data['target'] = y
	return data

def stroke():
	data = dict()
	df = pd.read_csv("data/stroke.csv")
	df = pd.get_dummies(df,columns=['gender','ever_married','work_type','Residence_type','smoking_status'])
	imputer = KNNImputer(missing_values=np.nan)
	tab = imputer.fit_transform(df)
	df_new = pd.DataFrame(tab, columns=df.columns)
	y = df_new["stroke"].astype(int)
	X = df_new.drop(["stroke"], axis = 1)
	X = pd.get_dummies(X)
	data['data'] = X
	data['target'] = y
	return data

def adult():
	data = dict()
	df = pd.read_csv("data/adult.data", header=None)
	df.columns = [
    "age",
    "workclass",
    "fnlwgt",
    "education",
    "education_num",
    "marital_status",
    "occupation",
    "relationship",
    "race",
    "sex",
    "capital_gain",
    "capital_loss",
    "hours_per_week",
    "native_country",
    "superior"
	]
	df = df.drop("native_country", axis=1)
	df["superior"] = df["superior"].apply(lambda x: "<=50K" if x == " <=50K." or x== " <=50K" else ">50K")
	df["superior"] = df["superior"].apply(lambda x: 0 if x == "<=50K" else 1)
	df = df.sample(n=1500, random_state=27).reset_index().drop("index", axis=1).drop_duplicates()
	df = pd.get_dummies(df)
	scale = StandardScaler()
	columns = df.columns
	y = df["superior"].copy()
	df = pd.DataFrame(scale.fit_transform(df))
	df.columns = columns
	df["superior"] = y.copy()
	data["target"] = df["superior"].copy().to_numpy(dtype=np.int64)
	data["data"] = df.drop('superior',axis=1).to_numpy()
	return data

def get_disease():
	data = pd.read_csv('data/heart.xls')
	data = dict()

	numerical_values=data[['Age', 'RestingBP', 'Cholesterol', 'MaxHR', 'Oldpeak']]    
	categorical_values=data[['Sex', 'ChestPainType','FastingBS', 'RestingECG', 'ExerciseAngina','ST_Slope', 'HeartDisease']]

	le = LabelEncoder()
	df1 = data.copy(deep = True)

	df1['Sex'] = le.fit_transform(df1['Sex'])
	df1['ChestPainType'] = le.fit_transform(df1['ChestPainType'])
	df1['RestingECG'] = le.fit_transform(df1['RestingECG'])
	df1['ExerciseAngina'] = le.fit_transform(df1['ExerciseAngina'])
	df1['ST_Slope'] = le.fit_transform(df1['ST_Slope'])
	
	normalize= MinMaxScaler()
	standard= StandardScaler()
	df1['Oldpeak']= normalize.fit_transform(df1[['Oldpeak']])
	df1['Age']= standard.fit_transform(df1[['Age']])
	df1['RestingBP']= standard.fit_transform(df1[['RestingBP']])
	df1['Cholesterol']= standard.fit_transform(df1[['Cholesterol']])
	df1['MaxHR']= standard.fit_transform(df1[['MaxHR']])
    
	data['data'] = df1[df1.columns.drop(['RestingECG', 'HeartDisease','RestingBP'])].values
	data['target'] = df1['HeartDisease'].values
	
	return data

def english():
	dir = 'Img'
	files = os.listdir(dir)
	datafile=[]
	data = dict()
	X=[]
	for file in files:
		image=load_img(os.path.join(dir,file),grayscale=False,color_mode='rgb',target_size=(100,100))
		image=img_to_array(image)
		image=image/255.0
		X+=[image]
		datafile+=[file]
	X = np.array(X)
	data['data']= X

	engl=pd.read_csv('english.csv')
	factlabel = pd.factorize(engl['label'])
	labelfile=[]
	for item in engl['image']:
		labelfile+=[item[4:]]
	engl['file']=labelfile
	engl['labeln']=factlabel[0]
	engl2=[]
	for item in datafile:
		engl2+=[engl['labeln'][engl['file']==item].values[0]]
	labels=to_categorical(engl2)
	labels=np.array(labels)

	data['target'] = labels
	return data


def date_fruit():
	df=pd.read_excel("Date_Fruit_Datasets/Date_Fruit_Datasets.xlsx")
	data = dict()
	labelencoder = LabelEncoder()
	X = df.drop("Class",axis=1).to_numpy()
	y = labelencoder.fit_transform(df['Class'].to_numpy())
	data['data'] = X
	data['target'] = y
	return data

def dry_bean():
	df = pd.read_excel('Dry_Bean_Dataset/Dry_Bean_Dataset.xlsx')
	labelencoder = LabelEncoder()
	data = dict()
	df["Class"] = labelencoder.fit_transform(df['Class'])
	X = df.drop(columns='Class').to_numpy()
	y = df['Class'].to_numpy()
	data['data'] = X
	data['target'] = y
	return data

def diseases_nlp():
	df = pd.read_csv('data/Symptom2Disease.csv').iloc[:,1:]
	model = SentenceTransformer('sentence-transformers/all-MiniLM-L6-v2')
	features = model.encode(df.iloc[:,1])
	r_targets = df.iloc[:,0].values
	le = LabelEncoder()
	targets = le.fit_transform(r_targets)
	data = dict()
	data['data'] = features
	data['target'] = targets
	return data

def mbti_nlp():
	df = pd.read_csv('data/twitter_MBTI.csv').iloc[:,1:-1]
	model = SentenceTransformer('sentence-transformers/all-MiniLM-L6-v2')
	features = model.encode(df.iloc[:,0])
	r_targets = df.i8loc[:,-1].values
	le = LabelEncoder()
	targets = le.fit_transform(r_targets)
	data = dict()
	data['data'] = features
	data['target'] = targets
	return data

def prep_tables(df):
	df = df.drop('Cabin',axis=1)
	df = df.drop(['PassengerId'],axis=1)
	df = df.drop(['Name'],axis=1)
	#train = train.drop(['age_group'],axis=1)
	df = df.drop(['Ticket'],axis=1)
	df.Age.fillna(train.Age.mean(),inplace=True)
	le=LabelEncoder()
	catcol = [col for col in df.columns if df[col].dtype == "object"] #encoding data
	for col in catcol:
		df[col]=le.fit_transform(df[col])
		X=df.drop(['Survived'],1)
		y=df.Survived

	return X,y

def get_titanic():
	train = pd.read_csv('Titanic/train.csv')
	X,y = prep_tables(train)
	return X,y

def glass():
	df = pd.read_csv('data/glass.csv')
	data = dict()
	data['data']= df.drop('Type',axis=1).to_numpy()
	data['target'] = LabelEncoder().fit_transform(df['Type'].to_numpy())
	return data

def ecoli():
	ecoli_df =  pd.read_csv("data/ecoli.csv",header=None,sep="\s+")
	col_names = ["squence_name","mcg","gvh","lip","chg","aac","alm1","alm2","site"]
	ecoli_df.columns = col_names
	def cleaning_object(ecoli_df,cols_to_drop,class_col):
		#ob1 suppose to be squence_name
		ecoli_df = ecoli_df.drop(cols_to_drop,axis=1)
			
		#drop classes with less than 10 instances
		uni_class = ecoli_df[class_col].unique().tolist()
		for class_label in uni_class:
			num_rows = sum(ecoli_df[class_col] == class_label)
			if num_rows < 10:
				class_todrop = ecoli_df[ecoli_df[class_col] == class_label].index
				ecoli_df.drop(class_todrop,inplace = True)
		return ecoli_df
	cleaned_ecoli_df = cleaning_object(ecoli_df,["squence_name",'lip','chg'],"site")
	scaler = StandardScaler()
	#Scale the cleaned data
	scaled_ecoli_df = scaler.fit_transform(cleaned_ecoli_df[['mcg','gvh','aac','alm1','alm2']])
	data = dict()
	data['data']= scaled_ecoli_df
	data['target'] = LabelEncoder().fit_transform(cleaned_ecoli_df['site'].to_numpy())
	return data

