"""
Sample code automatically generated on 2023-07-03 07:38:11

by geno from www.geno-project.org

from input

parameters
  vector upper
  vector lower
  vector est
variables
  vector p
min
  sum(p.*log(p./est))
st
  p <= upper
  p >= lower
  sum(p) == 1


The generated code is provided "as is" without warranty of any kind.
"""

from __future__ import division, print_function, absolute_import

from math import inf
from timeit import default_timer as timer
try:
    from genosolver import minimize, check_version
    USE_GENO_SOLVER = True
except ImportError:
    from scipy.optimize import minimize
    USE_GENO_SOLVER = False
    WRN = 'WARNING: GENO solver not installed. Using SciPy solver instead.\n' + \
          'Run:     pip install genosolver'
    print('*' * 63)
    print(WRN)
    print('*' * 63)



class GenoNLP:
    def __init__(self, upper, lower, est, np):
        self.np = np
        self.upper = upper
        self.lower = lower
        self.est = est
        assert isinstance(upper, self.np.ndarray)
        dim = upper.shape
        assert len(dim) == 1
        self.upper_rows = dim[0]
        self.upper_cols = 1
        assert isinstance(lower, self.np.ndarray)
        dim = lower.shape
        assert len(dim) == 1
        self.lower_rows = dim[0]
        self.lower_cols = 1
        assert isinstance(est, self.np.ndarray)
        dim = est.shape
        assert len(dim) == 1
        self.est_rows = dim[0]
        self.est_cols = 1
        self.p_rows = self.upper_rows
        self.p_cols = 1
        self.p_size = self.p_rows * self.p_cols
        # the following dim assertions need to hold for this problem
        assert self.upper_rows == self.lower_rows == self.p_rows == self.est_rows

    def getLowerBounds(self):
        bounds = []
        bounds += [-inf] * self.p_size
        return self.np.array(bounds)

    def getUpperBounds(self):
        bounds = []
        bounds += [inf] * self.p_size
        return self.np.array(bounds)

    def getStartingPoint(self):
        self.pInit = self.np.ones((self.p_rows, self.p_cols))/self.p_rows
        return self.pInit.reshape(-1)

    def variables(self, _x):
        p = _x
        return p

    def fAndG(self, _x):
        p = self.variables(_x)
        t_0 = (p / self.est)
        t_1 = self.np.log(t_0)
        f_ = self.np.sum((p * t_1))
        g_0 = (t_1 + ((p / t_0) / self.est))
        g_ = g_0
        return f_, g_

    def functionValueIneqConstraint000(self, _x):
        p = self.variables(_x)
        f = (p - self.upper)
        return f

    def gradientIneqConstraint000(self, _x):
        p = self.variables(_x)
        g_ = (self.np.eye(self.upper_rows, self.p_rows))
        return g_

    def jacProdIneqConstraint000(self, _x, _v):
        p = self.variables(_x)
        gv_ = (_v)
        return gv_

    def functionValueIneqConstraint001(self, _x):
        p = self.variables(_x)
        f = (self.lower - p)
        return f

    def gradientIneqConstraint001(self, _x):
        p = self.variables(_x)
        g_ = (-self.np.eye(self.p_rows, self.p_rows))
        return g_

    def jacProdIneqConstraint001(self, _x, _v):
        p = self.variables(_x)
        gv_ = (-_v)
        return gv_

    def functionValueEqConstraint000(self, _x):
        p = self.variables(_x)
        f = (self.np.sum(p) - 1)
        return f

    def gradientEqConstraint000(self, _x):
        p = self.variables(_x)
        g_ = (self.np.ones(self.p_rows))
        return g_

    def jacProdEqConstraint000(self, _x, _v):
        p = self.variables(_x)
        gv_ = ((_v * self.np.ones(self.p_rows)))
        return gv_

def solve(upper, lower, est, np):
    start = timer()
    NLP = GenoNLP(upper, lower, est, np)
    x0 = NLP.getStartingPoint()
    lb = NLP.getLowerBounds()
    ub = NLP.getUpperBounds()
    # These are the standard solver options, they can be omitted.
    options = {'eps_pg' : 1E-4,
               'constraint_tol' : 1E-4,
               'max_iter' : 3000,
               'm' : 10,
               'ls' : 0,
               'verbose' : 0  # Set it to 0 to fully mute it.
              }

    if USE_GENO_SOLVER:
        # Check if installed GENO solver version is sufficient.
        check_version('0.1.0')
        constraints = ({'type' : 'eq',
                        'fun' : NLP.functionValueEqConstraint000,
                        'jacprod' : NLP.jacProdEqConstraint000},
                       {'type' : 'ineq',
                        'fun' : NLP.functionValueIneqConstraint000,
                        'jacprod' : NLP.jacProdIneqConstraint000},
                       {'type' : 'ineq',
                        'fun' : NLP.functionValueIneqConstraint001,
                        'jacprod' : NLP.jacProdIneqConstraint001})
        result = minimize(NLP.fAndG, x0, lb=lb, ub=ub, options=options,
                      constraints=constraints, np=np)
    else:
        # SciPy: for inequality constraints need to change sign f(x) <= 0 -> f(x) >= 0
        constraints = ({'type' : 'eq',
                        'fun' : NLP.functionValueEqConstraint000,
                        'jac' : NLP.gradientEqConstraint000},
                       {'type' : 'ineq',
                        'fun' : lambda x: -NLP.functionValueIneqConstraint000(x),
                        'jac' : lambda x: -NLP.gradientIneqConstraint000(x)},
                       {'type' : 'ineq',
                        'fun' : lambda x: -NLP.functionValueIneqConstraint001(x),
                        'jac' : lambda x: -NLP.gradientIneqConstraint001(x)})
        result = minimize(NLP.fAndG, x0, jac=True, method='SLSQP',
                          bounds=list(zip(lb, ub)),
                          constraints=constraints)

    # assemble solution and map back to original problem
    p = NLP.variables(result.x)
    #elapsed = timer() - start
    #print('solving took %.3f sec' % elapsed)
    return result, p

def generateRandomData(np):
    np.random.seed(0)
    sample = 1- np.random.rand(3) 
    upper = sample/np.min(sample)
    lower = sample/np.max(sample)
    est = 1- np.random.rand(3)
    return upper, lower, est

if __name__ == '__main__':
    import numpy as np
    #import cupy as np  # uncomment this for GPU usage
    print('\ngenerating random instance')
    upper, lower, est = generateRandomData(np=np)
    print('solving ...')
    result, p = solve(upper, lower, est, np=np)
