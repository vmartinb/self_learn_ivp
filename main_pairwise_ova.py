import sys
sys.path.append('/home/vitorbordini/Downloads/self_learn_ivp')
import wandb
import pandas as pd
import numpy as np
from sklearn.metrics import accuracy_score,confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler,normalize
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import RandomForestRegressor
from sklearn.utils import shuffle
import matplotlib.pyplot as plt
import seaborn as sns
from sklearn.ensemble import IsolationForest
from tqdm import tqdm
np.seterr(divide='raise')
from openpyxl import load_workbook,Workbook
from multiprocessing import Pool
from decimal import Decimal

import torch
from torch import nn
from torch.nn import functional as F
from torch.autograd import Variable
from collections import OrderedDict
import tensorflow as tf


import cvxpy as cp
import os
from neural_net import SimpleNeuralNet,PairwiseNeuralNetOVA,init_weights
from utils import plot_decision_boundary,plot_point,dataset,expected_calibration_error
from taxonomies_calibration import *
from optimistic_solver import solve
from datasetsml import *
from parserml import argpar

#['CBC', 'CVXOPT', 'ECOS', 'ECOS_BB', 'GLOP', 'GLPK', 'GLPK_MI', 'GUROBI', 'MOSEK', 'OSQP', 'PDLP', 'SCIPY', 'SCS']
solver = 'ECOS'
def solve_cvx(param):
    pairwise_bounds = param['bounds']
    pred = param['pred']
    num_classes = pred.shape[0]
    upper = pairwise_bounds[:,0]
    lower = pairwise_bounds[:,1]
    if np.sum(lower)>1:
        deltas = cp.Variable(num_classes)
        cts = [deltas>=0,deltas<=lower,(1-np.eye(num_classes))@(lower-deltas)+upper<=1]
        pb = cp.Problem(cp.Minimize(cp.sum(deltas)),cts)
        pb.solve(solver='SCS')
        lower = lower-deltas.value
    if np.sum(upper)<1:
        deltas = cp.Variable(num_classes)
        cts = [deltas>=0,deltas<=1-upper,(1-np.eye(num_classes))@(upper+deltas)+lower>=1]
        pb = cp.Problem(cp.Minimize(cp.sum(deltas)),cts)
        pb.solve(solver='SCS')
        upper = upper+deltas.value
    p_e = cp.Variable(num_classes)
    prob = cp.Problem(cp.Minimize(cp.sum(cp.rel_entr(p_e,pred))),
                    [p_e<=upper,p_e>=lower,cp.sum(p_e)==1])
    try:
        prob.solve(solver=solver,max_iters=10000,verbose=False)
    except cp.error.SolverError:
        prob.solve(solver='SCS',max_iters=10000,verbose=False)
    if p_e.value is None:
        print('p is None')
        return pred
    return p_e.value


def one_hot(vector,num_classes=3):
    vector = vector.squeeze()
    b = np.zeros((vector.size,num_classes)).astype(np.int64)
    b[np.arange(vector.size), vector] = 1
    smooth_factor = 1/(num_classes*num_classes)
    b = (1-smooth_factor)*b + smooth_factor
    return b

if __name__=='__main__':
    wandb.login()
    par = argpar()
    data,n_hidden_units = dataset(par)
    device = torch.device('cuda:'+str(par.cuda) if par.cuda!=-1 else torch.device('cpu'))


    run = wandb.init(
        mode='disabled',
        # Set the project where this run will be logged
        project="my-awesome-project",
        # Track hyperparameters and run metadata
        config={
            "learning_rate": par.lr,
            "dataset": par.dataset,
            "rep_per_class":par.rep_per_class,
            "labeled_instances": 20
        })

    X,y = data['data'],data['target']
    X = normalize(X)
    num_classes = np.max(y)+1

    n_instances,n_input = X.shape
    y = one_hot(y,num_classes=num_classes).squeeze()
    #X,y = shuffle(X,y)
    instances_per_class =(((num_classes - np.arange(num_classes))/num_classes) * (n_instances/num_classes)).astype(int)
    instances_per_class[0]-= 1
    #print(instances_per_class)
    indexes = []
    for cl in range(num_classes):
        indexes.append(np.where(y[:,cl]==1)[0][:instances_per_class[cl]])
    indexes = np.concatenate(indexes)
    #X,y = X[indexes],y[indexes]
    '''
    if X.shape[1]==2:
        for label in range(num_classes):
            indexes = np.where(y[:,label]==1)[0]
            plt.scatter(X[indexes,0],X[indexes,1],label='class_{}'.format(label))

            
        plt.legend()
        plt.savefig('dataset')
    '''
    state_dicts = {}
    for seed in np.linspace(start=1,stop=100,num=par.seeds_num).astype(int):
        torch.manual_seed(seed)
        state_dicts[seed]  = PairwiseNeuralNetOVA(0.01,n_input,n_hidden_units,num_classes=num_classes,device=device).state_dict()

    def number2letter(num):
        '''
        0->'A'
        1->'B'
        etc
        '''
        num2add = ord('A')
        return chr(num2add + num)
    #print(cp.installed_solvers())
        
    def self_learn(par,X,y,taxonomy=None,st='soft',epochs=50,iterations=50):
        '''
        Self-training algorithm
            Args:
                X(ndarray, shape (N,M)): inputs
                y(ndarray, shape (N,num_classes)): outputs (hard labels)
                taxonomy(function): Venn taxonomy in case of IVP.
                lr(float): learning rate for training.
                st(string): Strategy implemented.
                epochs(int): Number of epochs to train the model on each iteration.
                iterations(int): Number of maximum iterations.
        '''
        lr = par.lr
        split = int(par.split_size*X.shape[0])
        print(split)
        num=par.seeds_num
        seeds = np.linspace(start=1,stop=100,num=num).astype(int)
        num_classes = y.shape[1]
        accuracies = np.zeros((iterations+1,len(seeds)))
        eces = np.zeros((iterations+1,len(seeds)))
        accuracies_per_class = np.zeros((iterations+1,len(seeds),num_classes))
        rep_per_class= par.rep_per_class
        loss_value_test = torch.Tensor([]).to(device)
        loss_value_train = torch.Tensor([]).to(device)
        f = open('pairwise_ova/{}/result_{}_{}_{}_parameters.txt'.format(par.dataset,par.rep_per_class*num_classes,split,st), "w")
        f.write(str(par))
        f.close()

        print(X.shape,y.shape)
        file_path = 'pairwise_ova/{}/result_{}_{}_{}_min.xlsx'.format(par.dataset,par.rep_per_class*num_classes,split,st)
        soft=True
        if os.path.exists(file_path):
            writer = load_workbook(file_path,read_only=False)
            acc_writer = writer['accuracies']
            ece_writer = writer['eces']
        else:
            writer = Workbook()
            acc_writer = writer.create_sheet("accuracies")
            ece_writer = writer.create_sheet("eces")
        for ii,seed in enumerate(seeds):
            acc_writer[number2letter(ii)+'1'].value = seed
            ece_writer[number2letter(ii)+'1'].value = seed
        for i,seed in enumerate(seeds):
            X_train, X_test, y_train, y_test = train_test_split(X,y,test_size=.2,random_state=seed)
            prop_per_class = np.unique(np.argmax(y,axis=1),return_counts=True)[1]/len(y)
            prop_per_class = np.round((prop_per_class/np.min(prop_per_class))).astype(int)
            torch.manual_seed(seed)
            model = PairwiseNeuralNetOVA(0.01,n_input,n_hidden_units,num_classes=num_classes,device=device)
            model.load_state_dict(state_dicts[seed])
            sum_prop = np.sum(prop_per_class)
            indexes = []
            for cl in range(num_classes):
                indexes.append(np.where(y_train[:,cl]>=0.5)[0][:prop_per_class[cl]*split//sum_prop])
            indexes = np.concatenate(indexes)
            X_labeled,y_labeled = X_train[indexes],y_train[indexes]
            X_unlabeled = np.delete(X_train,indexes,0)
            y_unlabeled = np.delete(y_train,indexes,0)
            X_labeled,y_labeled = shuffle(X_labeled,y_labeled)

            #aux = X_unlabeled[y==num_classes-1]
            #point_to_plot = aux[0]  

            prop = int(len(X_unlabeled)/iterations)
            cond = st == 'credal'
            if cond:
                indexes = []
                for cl in range(num_classes):
                        indexes.append(np.where(y_labeled[:,cl]>=0.5)[0][:rep_per_class])
                indexes = np.concatenate(indexes)
                X_cal,y_cal = X_labeled[indexes],y_labeled[indexes]
                X_labeled = np.delete(X_labeled,indexes,0)
                y_labeled = np.delete(y_labeled,indexes,0)
                X_cal,y_cal = shuffle(X_cal,y_cal)
            model.fit(X_labeled,y_labeled,epochs,lr,verbose=False)
            test_pred = model.predict_proba(X_test,as_numpy=False).float().cpu().detach().numpy()
            matrix = confusion_matrix(y_true=np.argmax(y_test,axis=1),
                                        y_pred=np.argmax(test_pred,axis=1))
            test_pred = model.predict_proba(X_test,as_numpy=False).float().cpu().detach().numpy()
            accuracies[0,i] = np.sum(np.diag(matrix))/np.sum(matrix)
            accuracies_per_class[0,i,:]=(matrix.astype('float') / matrix.sum(axis=1)[:, np.newaxis]).diagonal()
            #print(accuracy_score(y_true=np.argmax(y_train.detach().cpu().numpy(),axis=1),y_pred=np.argmax(train_pred.detach().cpu().numpy(),axis=1)))
            eces[0,i] =  expected_calibration_error(test_pred,y_test,num_classes=num_classes)
            acc_writer[number2letter(i)+str(0+2)].value = accuracies[0,i]
            ece_writer[number2letter(i)+str(0+2)].value = eces[0,i]
            writer.save(file_path)
            torch.cuda.empty_cache()
            threshold = 0.8
            for j in tqdm(range(iterations)):
                if st =='hard':
                    y_unlabeled = model.predict_proba(X_unlabeled,as_numpy=True)
                    probs_to_add = np.max(y_unlabeled,axis=1)>threshold
                    
                    y_labeled = np.vstack((y_labeled,y_unlabeled[probs_to_add]))
                    y_unlabeled = y_unlabeled[np.logical_not(probs_to_add)]
                    X_labeled = np.vstack((X_labeled,X_unlabeled[probs_to_add]))
                    y_unlabeled = one_hot(np.argmax(y_unlabeled,axis=1),num_classes)
                    X_unlabeled = X_unlabeled[np.logical_not(probs_to_add)]
                    y_acc = y_labeled#torch.vstack((y_labeled,y_unlabeled))
                    X_acc = X_labeled#torch.vstack((X_labeled,X_unlabeled[j*prop:(j+1)*prop]))
                    soft=False

                elif cond:
                    y_unlabeled = model.predict_proba(X_unlabeled,as_numpy=True)
                    for ii in range(len(y_unlabeled)):
                        y_unlabeled[ii] = y_unlabeled[ii]/np.sum(y_unlabeled[ii])

                    e = 0.01
                    y_unlabeled = (1-e)*y_unlabeled + e/num_classes
                    pred_cal = model.predict_proba(X_cal, as_numpy=True)
                    pred_cal = (1-e)*pred_cal + e/num_classes
                    params_list = []
                    probs_to_add = np.max(y_unlabeled,axis=1)>threshold
                    y_probs = y_unlabeled[probs_to_add]
                    X_probs = X_unlabeled[probs_to_add]
                    for k in range(len(X_probs)):
                        params_list.append(dict())
                        pairwise_bounds = model.multi_class_IVAP(X_cal,y_cal,X_probs[k])
                        pairwise_bounds = (1-e)*pairwise_bounds + e/num_classes
                        params_list[-1]['bounds'] = pairwise_bounds
                        params_list[-1]['pred'] = y_probs[k].copy()
                        upper = pairwise_bounds[:,0]
                        lower = pairwise_bounds[:,1]
                        np.where(y_unlabeled[k] > upper,upper,y_unlabeled[k]) 
                        np.where(y_unlabeled[k] < lower,lower,y_unlabeled[k])

                        #y_probs[k] = solve_cvx(params_list[-1])
                    
                    y_labeled = np.vstack((y_labeled,y_probs))
                    y_unlabeled = y_unlabeled[np.logical_not(probs_to_add)]
                    X_labeled = np.vstack((X_labeled,X_probs))
                    X_unlabeled = X_unlabeled[np.logical_not(probs_to_add)]
                    y_acc = y_labeled#torch.vstack((y_labeled,y_unlabeled))
                    X_acc = X_labeled#torch.vstack((X_labeled,X_unlabeled[j*prop:(j+1)*prop]))

                elif st == 'soft':
                    y_unlabeled = model.predict_proba(X_unlabeled)
                    probs_to_add = np.max(y_unlabeled,axis=1)>threshold
                    y_labeled = np.vstack((y_labeled,y_unlabeled[probs_to_add]))
                    y_unlabeled = y_unlabeled[np.logical_not(probs_to_add)]
                    X_labeled = np.vstack((X_labeled,X_unlabeled[probs_to_add]))
                    X_unlabeled = X_unlabeled[np.logical_not(probs_to_add)]
                    y_acc = y_labeled#np.vstack((y_labeled,y_unlabeled))
                    X_acc = X_labeled#np.vstack((X_labeled,X_unlabeled))
                    soft=True
                    for ii in range(len(y_acc)):
                        y_acc[ii] = y_acc[ii]/np.sum(y_acc[ii])
                    
                    y_acc = y_labeled#torch.vstack((y_labeled,y_unlabeled))
                    X_acc = X_labeled#torch.vstack((X_labeled,X_unlabeled[j*prop:(j+1)*prop]))
                test_pred = model.predict_proba(X_test,as_numpy=False).float().cpu().detach().numpy()
                for jj in range(len(test_pred)):
                    test_pred[jj] = test_pred[jj]/np.sum(test_pred[jj])
                model.fit(X_acc,y_acc,epochs,lr,verbose=False,soft=soft)
                y_test_nonzero = y_test.copy().astype(np.float64)
                y_test_nonzero[y_test_nonzero==0]+= 0.001
                y_test_nonzero[y_test_nonzero==1]-= 0.001
                loss_value_test = np.hstack((loss_value_test,(y_test_nonzero*(np.log(y_test_nonzero+0.001)-np.log(test_pred+0.001))).sum(axis=1).mean()))
                plt.plot(loss_value_test)
                plt.savefig('pairwise_ova/{}/loss_test_{}_pairwise_min'.format(par.dataset,st))
                plt.clf()
                
                y_train_nonzero = y_train.copy().astype(np.float64)
                y_train_nonzero[y_train_nonzero==0]+= 0.001
                y_train_nonzero[y_train_nonzero==1]-= 0.001
                train_pred = model.predict_proba(X_train,as_numpy=False).float().cpu().detach().numpy()
                loss_value_train = np.hstack((loss_value_train,(y_train_nonzero*(np.log(y_train_nonzero+0.001)-np.log(train_pred+0.001))).sum(axis=1).mean()))
                plt.plot(loss_value_train)
                plt.savefig('pairwise_ova/{}/loss_train_{}_pairwise_min'.format(par.dataset,st))
                plt.clf()
                torch.save(model.state_dict(),'pairwise_ova/{}/model_{}_{}_{}_{}_min.pth'.format(par.dataset,par.rep_per_class*num_classes,split,j,st))
                matrix = confusion_matrix(y_true=np.argmax(y_test,axis=1),
                                            y_pred=np.argmax(test_pred,axis=1))

                accuracies[j+1,i] = np.sum(np.diag(matrix))/np.sum(matrix)
                accuracies_per_class[j,i,:]=(matrix.astype('float') / matrix.sum(axis=1)[:, np.newaxis]).diagonal()
                #print(accuracy_score(y_true=np.argmax(y_train.detach().cpu().numpy(),axis=1),y_pred=np.argmax(train_pred.detach().cpu().numpy(),axis=1)))
                eces[j+1,i] =  expected_calibration_error(test_pred,y_test,num_classes=num_classes)
                acc_writer[number2letter(i)+str(j+3)].value = accuracies[j,i]
                ece_writer[number2letter(i)+str(j+3)].value = eces[j,i]
                writer.save(file_path)
                torch.cuda.empty_cache()
        '''
        mean_accuracies = np.mean(accuracies,axis=1).astype('<U5')
        std_accuracies =  np.std(accuracies,axis=1).astype('<U5')
        mean_eces = np.mean(eces,axis=1).astype('<U5')
        std_eces = np.std(eces,axis=1).astype('<U5')
        sep=np.array(['+/-']*len(mean_accuracies))
        print_accuracies = np.char.add(np.char.add(mean_accuracies,sep),std_accuracies)
        print_eces = np.char.add(np.char.add(mean_eces,sep),std_eces)
        with pd.ExcelWriter('pairwise_ova/{}/result_{}_{}_{}_min.xlsx'.format(par.dataset,par.rep_per_class*num_classes,split,st), engine='xlsxwriter') as writer:
            pd.DataFrame(print_accuracies).to_excel(writer,sheet_name='accuracies')
            pd.DataFrame(print_eces).to_excel(writer,sheet_name='eces')
        '''

        return  np.mean(accuracies,axis=1),np.mean(accuracies_per_class,axis=1),np.mean(eces,axis=1)
    
    def compare_self_learn(par,X,y,iterations):
        if not os.path.exists('pairwise'):
            os.mkdir('pairwise_ova')
        if not os.path.exists('pairwise_ova/' + par.dataset):
            os.mkdir('pairwise_ova/' + par.dataset)

        dec = Decimal(str(par.lr))
        stlr = '{:.2e}'.format(dec).replace('.','').replace('0','')

        acc_cred_opt,acc_per_class_cred_opt,eces_credal = self_learn(par,X,y,st='credal',iterations=iterations)
        print(acc_cred_opt)

        acc_hard,acc_per_class_hard,eces_hard = self_learn(par,X,y,st='hard',iterations=iterations)
        print(acc_hard)

        acc_soft,acc_per_class_soft,eces_soft = self_learn(par,X,y,st='soft',iterations=30)
        print(acc_soft)
        
        plt.figure()
        plot_dict = {'hard':acc_hard, 'soft':acc_soft, 'credal':acc_cred_opt}
        plot = pd.DataFrame(plot_dict)
        wandb.log(plot_dict)
        sns.lineplot(data=plot).set(title="Accuracy")
        plt.savefig('pairwise_ova/{}/result_{}_{}_pairwise_min'.format(par.dataset,par.rep_per_class*num_classes,str(int(par.split_size*X.shape[0]))))

        plt.figure()
        plot_dict = {'hard':eces_hard, 'soft':eces_soft, 'credal_opt':eces_credal}
        plot = pd.DataFrame(plot_dict)
        wandb.log(plot_dict)
        sns.lineplot(data=plot).set(title="ECE")
        plt.savefig('pairwise_ova/{}/result_{}_{}_pairwise_min_ece'.format(par.dataset,par.rep_per_class*num_classes,str(int(par.split_size*X.shape[0]))))
        for cl in range(num_classes):
            plt.figure()
            plot_dict_class = {'hard_'+str(cl):acc_per_class_hard[:,cl], 'soft_'+str(cl):acc_per_class_soft[:,cl], 'credal'+str(cl):acc_per_class_cred_opt[:,cl],
                            }
            wandb.log(plot_dict_class)
            plot = pd.DataFrame(plot_dict_class)
            sns.lineplot(data=plot).set(title="Accuracy")
            plt.savefig('pairwise_ova/{}/result_{}_class_{}_pairwise_min'.format(par.dataset,par.rep_per_class*num_classes,cl))

            #plt.xlim([0,1])
            #plt.ylim([0,0.6])
        #plot_point(venn_pred,predictions,p_opt)
    compare_self_learn(par,X,y,iterations=30)
