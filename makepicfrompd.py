import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from parserml import argpar
from utils import dataset
import numpy as np

par = argpar()
data,n_hidden_units = dataset(par)
num_classes = np.max(data['target'])+1
split_size=int(data['data'].shape[0]*par.split_size)
aux = pd.read_excel('pairwise_ova/{}/result_{}_{}_credal_min.xlsx'.format(par.dataset,par.rep_per_class*num_classes,split_size),sheet_name=[1,2])
acc_credal = aux[1].mean(axis=1).to_numpy().flatten()
eces_credal = aux[2].mean(axis=1).to_numpy().flatten()

aux = pd.read_excel('pairwise_ova/{}/result_{}_{}_soft_min.xlsx'.format(par.dataset,par.rep_per_class*num_classes,split_size),sheet_name=[1,2])
acc_soft = aux[1].mean(axis=1).to_numpy().flatten()
eces_soft = aux[2].mean(axis=1).to_numpy().flatten()

aux = pd.read_excel('pairwise_ova/{}/result_{}_{}_hard_min.xlsx'.format(par.dataset,par.rep_per_class*num_classes,split_size),sheet_name=[1,2])
acc_hard = aux[1].mean(axis=1).to_numpy().flatten()
eces_hard = aux[2].mean(axis=1).to_numpy().flatten()

plt.figure()
plot_dict = {'hard':acc_hard, 'soft':acc_soft, 'credal':acc_credal}
print(plot_dict)
plot = pd.DataFrame(plot_dict)
sns.lineplot(data=plot).set(title="Accuracy")
plt.savefig('pairwise_ova/{}/result_{}_{}_min'.format(par.dataset,par.rep_per_class*num_classes,split_size))


plt.figure()
plot_dict = {'hard':eces_hard, 'soft':eces_soft, 'credal_opt':eces_credal}
plot = pd.DataFrame(plot_dict)
sns.lineplot(data=plot).set(title="ECE")
plt.savefig('pairwise_ova/{}/result_{}_{}_min_ece'.format(par.dataset,par.rep_per_class*num_classes,split_size))
