from sklearn.model_selection import train_test_split
import numpy as np
from sklearn.preprocessing import LabelEncoder,MinMaxScaler, StandardScaler
import pandas as pd
from sentence_transformers import SentenceTransformer
import os
from tensorflow.keras.preprocessing.image import img_to_array,load_img
from tensorflow.keras.utils import to_categorical
from tensorflow import keras
from sklearn.impute import KNNImputer

def fraud():
	data = dict()
	df = pd.read_csv("data/Base.csv")
	y = df["fraud_bool"]
	X = df.drop(["fraud_bool", "device_os", "device_fraud_count", "month"], axis = 1)
	data['data'] = X
	data['target'] = y
	return data

def stroke():
	data = dict()
	df = pd.read_csv("data/stroke.csv")
	df = pd.get_dummies(df,columns=['gender','ever_married','work_type','Residence_type','smoking_status'])
	imputer = KNNImputer(missing_values=np.nan)
	tab = imputer.fit_transform(df)
	df_new = pd.DataFrame(tab, columns=df.columns)
	y = df_new["stroke"].astype(int)
	X = df_new.drop(["stroke"], axis = 1)
	X = pd.get_dummies(X)
	data['data'] = X
	data['target'] = y
	return data

def adult():
	data = dict()
	df = pd.read_csv("data/adult.data", header=None)
	df.columns = [
    "age",
    "workclass",
    "fnlwgt",
    "education",
    "education_num",
    "marital_status",
    "occupation",
    "relationship",
    "race",
    "sex",
    "capital_gain",
    "capital_loss",
    "hours_per_week",
    "native_country",
    "superior"
	]
	df = df.drop("native_country", axis=1)
	df["superior"] = df["superior"].apply(lambda x: "<=50K" if x == " <=50K." or x== " <=50K" else ">50K")
	df["superior"] = df["superior"].apply(lambda x: 0 if x == "<=50K" else 1)
	df = df.sample(n=1500, random_state=27).reset_index().drop("index", axis=1).drop_duplicates()
	df = pd.get_dummies(df)
	scale = StandardScaler()
	columns = df.columns
	y = df["superior"].copy()
	df = pd.DataFrame(scale.fit_transform(df))
	df.columns = columns
	df["superior"] = y.copy()
	data["target"] = df["superior"].copy().to_numpy(dtype=np.int64)
	data["data"] = df.drop('superior',axis=1).to_numpy()
	return data

def get_disease():
	table = pd.read_csv('data/heart.xls')
	

	numerical_values=table[['Age', 'RestingBP', 'Cholesterol', 'MaxHR', 'Oldpeak']]    
	categorical_values=table[['Sex', 'ChestPainType','FastingBS', 'RestingECG', 'ExerciseAngina','ST_Slope', 'HeartDisease']]

	le = LabelEncoder()
	df1 = table.copy(deep = True)

	df1['Sex'] = le.fit_transform(df1['Sex'])
	df1['ChestPainType'] = le.fit_transform(df1['ChestPainType'])
	df1['RestingECG'] = le.fit_transform(df1['RestingECG'])
	df1['ExerciseAngina'] = le.fit_transform(df1['ExerciseAngina'])
	df1['ST_Slope'] = le.fit_transform(df1['ST_Slope'])
	
	normalize= MinMaxScaler()
	standard= StandardScaler()
	df1['Oldpeak']= normalize.fit_transform(df1[['Oldpeak']])
	df1['Age']= standard.fit_transform(df1[['Age']])
	df1['RestingBP']= standard.fit_transform(df1[['RestingBP']])
	df1['Cholesterol']= standard.fit_transform(df1[['Cholesterol']])
	df1['MaxHR']= standard.fit_transform(df1[['MaxHR']])

	data = dict()
	data['data'] = df1[df1.columns.drop(['RestingECG', 'HeartDisease','RestingBP'])].values
	data['target'] = df1['HeartDisease'].values
	
	return data

def english():
	dir = 'Img'
	files = os.listdir(dir)
	datafile=[]
	data = dict()
	X=[]
	for file in files:
		image=load_img(os.path.join(dir,file),grayscale=False,color_mode='rgb',target_size=(100,100))
		image=img_to_array(image)
		image=image/255.0
		X+=[image]
		datafile+=[file]
	X = np.array(X)
	data['data']= X

	engl=pd.read_csv('english.csv')
	factlabel = pd.factorize(engl['label'])
	labelfile=[]
	for item in engl['image']:
		labelfile+=[item[4:]]
	engl['file']=labelfile
	engl['labeln']=factlabel[0]
	engl2=[]
	for item in datafile:
		engl2+=[engl['labeln'][engl['file']==item].values[0]]
	labels=to_categorical(engl2)
	labels=np.array(labels)

	data['target'] = labels
	return data


def date_fruit():
	df=pd.read_excel("data/Date_Fruit_Datasets/Date_Fruit_Datasets.xlsx")
	data = dict()
	labelencoder = LabelEncoder()
	X = df.drop("Class",axis=1).to_numpy()
	y = labelencoder.fit_transform(df['Class'].to_numpy())
	data['data'] = X
	data['target'] = y
	return data

def dry_bean():
	df = pd.read_excel('data/DryBeanDataset/Dry_Bean_Dataset.xlsx')
	labelencoder = LabelEncoder()
	data = dict()
	df["Class"] = labelencoder.fit_transform(df['Class'])
	X = df.drop(columns='Class').to_numpy()
	y = df['Class'].to_numpy()
	data['data'] = X
	data['target'] = y
	return data

def diseases_nlp():
	df = pd.read_csv('data/Symptom2Disease.csv').iloc[:,1:]
	model = SentenceTransformer('sentence-transformers/all-MiniLM-L6-v2')
	features = model.encode(df.iloc[:,1])
	r_targets = df.iloc[:,0].values
	le = LabelEncoder()
	targets = le.fit_transform(r_targets)
	data = dict()
	data['data'] = features
	data['target'] = targets
	return data

def mbti_nlp():
	df = pd.read_csv('data/twitter_MBTI.csv').iloc[:,1:-1]
	model = SentenceTransformer('sentence-transformers/all-MiniLM-L6-v2')
	features = model.encode(df.iloc[:,0])
	r_targets = df.i8loc[:,-1].values
	le = LabelEncoder()
	targets = le.fit_transform(r_targets)
	data = dict()
	data['data'] = features
	data['target'] = targets
	return data

def prep_tables(df):
	df = df.drop('Cabin',axis=1)
	df = df.drop(['PassengerId'],axis=1)
	df = df.drop(['Name'],axis=1)
	#train = train.drop(['age_group'],axis=1)
	df = df.drop(['Ticket'],axis=1)
	df.Age.fillna(df.Age.mean(),inplace=True)
	le=LabelEncoder()
	catcol = [col for col in df.columns if df[col].dtype == "object"] #encoding data
	for col in catcol:
		df[col]=le.fit_transform(df[col])
		X=df.drop(['Survived'],1)
		y=df.Survived

	return X,y

def get_titanic():
	train = pd.read_csv('data/titanic.csv')
	X,y = prep_tables(train)
	data = dict()
	data['data'] = X
	data['target'] = y
	return data

def glass():
	df = pd.read_csv('data/glass.csv')
	data = dict()
	data['data']= df.drop('Type',axis=1).to_numpy()
	data['target'] = LabelEncoder().fit_transform(df['Type'].to_numpy())
	return data

def ecoli():
	ecoli_df =  pd.read_csv("data/ecoli.csv",header=None,sep="\s+")
	col_names = ["squence_name","mcg","gvh","lip","chg","aac","alm1","alm2","site"]
	ecoli_df.columns = col_names
	def cleaning_object(ecoli_df,cols_to_drop,class_col):
		#ob1 suppose to be squence_name
		ecoli_df = ecoli_df.drop(cols_to_drop,axis=1)
			
		#drop classes with less than 10 instances
		uni_class = ecoli_df[class_col].unique().tolist()
		for class_label in uni_class:
			num_rows = sum(ecoli_df[class_col] == class_label)
			if num_rows < 10:
				class_todrop = ecoli_df[ecoli_df[class_col] == class_label].index
				ecoli_df.drop(class_todrop,inplace = True)
		return ecoli_df
	cleaned_ecoli_df = cleaning_object(ecoli_df,["squence_name",'lip','chg'],"site")
	scaler = StandardScaler()
	#Scale the cleaned data
	scaled_ecoli_df = scaler.fit_transform(cleaned_ecoli_df[['mcg','gvh','aac','alm1','alm2']])
	data = dict()
	data['data']= scaled_ecoli_df
	data['target'] = LabelEncoder().fit_transform(cleaned_ecoli_df['site'].to_numpy())
	return data

def load_mixture_dataset(n_example: int,num_classes:int):
    """Create and return a mock dataset. If we plot the data, we can see
    a sort of flower. The created dataset contains n_example X of 2 features
    and their associated label

    Args:
        n_example (int): number of examples in the wanted dataset

    Returns:
        Tuple[np.ndarray, np.ndarray]: Features matrix and associated labels vector
    """
    np.random.seed(1)
    n_representation = int(n_example/num_classes)
    n_features = 2
    features = np.zeros((n_example, n_features))
    labels = np.zeros((n_example,1), dtype='uint8')
    
    for label in range(num_classes):
        mean = 6*label*np.ones(2)
        cov = [[5.5,(-1)**label*2*label],[(-1)**label*2*label,5.5]]
        index = range(n_representation*label,n_representation*(label+1))
        features[index] = np.random.multivariate_normal(mean, cov, size=n_representation)
        labels[index] = label
    data = dict()
    data['target'] = labels
    data['data'] = features
    return data


def letter_reco():
	data = dict()
	aux = pd.read_csv('data/letter_reco/dataset.csv').iloc[:5000]
	labels = LabelEncoder().fit_transform(aux['target'])
	features = aux.drop(['target'],axis=1).to_numpy()
	data['target'] = labels
	data['data'] = features

	return data


def steel_plates():
	data = dict()
	aux = pd.read_csv('data/steel_plates.csv').to_numpy()
	features = aux[:,:-1]
	labels = aux[:,-1].copy()
	for ii in [0,1,3,4]:
		features = np.delete(features,labels==ii,axis=0)
		labels = np.delete(labels,labels==ii)
	labels = LabelEncoder().fit_transform(labels)
	data['target'] = labels
	data['data'] = features

	return data

def australian():
	data = dict()
	df = pd.read_csv("data/australian.dat", sep="\s+", header=None)
	df.columns = ["X1", "X2", "X3", "X4", "X5", "X6", "X7", "X8", "X9", "X10", "X11", "X12", "X13", "X14", "target"]
	data['data']= df.drop('target',axis=1).to_numpy()
	data['target'] = LabelEncoder().fit_transform(df['target'].to_numpy())
	return data

def poem():
	data = dict()
	model = SentenceTransformer('sentence-transformers/all-MiniLM-L6-v2')
	df = pd.read_csv("data/poem.csv")
	df['age']= LabelEncoder().fit_transform(df['age'])
	df['type']= LabelEncoder().fit_transform(df['type'])
	df['poem content'] = model.encode(df['poem content'])
	data['data']= df.drop(['type','label'],axis=1).to_numpy()
	data['target'] = LabelEncoder().fit_transform(df['label'].to_numpy())
	return data

def wall():
	data = dict()
	df = pd.read_csv("data/wall_following.csv")
	df['class']= LabelEncoder().fit_transform(df['class'])
	data['target'] = LabelEncoder().fit_transform(df['class'].to_numpy())
	data['data']= df.drop(['class'],axis=1).to_numpy()
	return data

def heloc():
	data = dict()
	df = pd.read_csv("data/heloc/heloc.csv")
	df['RiskPerformance']= LabelEncoder().fit_transform(df['RiskPerformance'])
	data['target'] = LabelEncoder().fit_transform(df['RiskPerformance'].to_numpy())
	data['data']= df.drop(['RiskPerformance'],axis=1).to_numpy()
	return data

def mnist():
	num_samples = 10000
	(x_train, y_train), (x_test, y_test) = keras.datasets.mnist.load_data()
	y_train = y_train.reshape((-1,1))
	y_test = y_test.reshape((-1,1))
	data = dict()
	data['target'] = np.vstack((y_train,y_test))#[:num_samples]
	data['data']= np.vstack((x_train,x_test))#[:num_samples]
	return data


def cifar10():
	num_samples = 500
	(x_train, y_train), (x_test, y_test) = keras.datasets.cifar10.load_data()
	y_train = y_train.reshape((-1,1))
	y_test = y_test.reshape((-1,1))
	data = dict()
	data['target'] = np.vstack((y_train,y_test))
	data['data']= np.vstack((x_train,x_test))
	return data
