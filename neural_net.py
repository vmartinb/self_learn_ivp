"""This file contains declaration of 2 neural network classes (one for hard
and soft labels, the other for hard and credal labels).

Self Learning using Venn Abers predictors

@Côme Rodriguez, @Vitor Bordini, @Sébastien Destercke and @Benjamin Quost
"""

from typing import Union
from sklearn.metrics import accuracy_score
import numpy as np
import pandas as pd
import torch
from torch import nn
from collections import OrderedDict
import tensorflow as tf
import torch.optim.lr_scheduler as lr_scheduler
from venn_abers import venn_abers_pytorch
import torch.nn.functional as F
import matplotlib.pyplot as plt


def init_weights(m):
    if isinstance(m, nn.Linear):
        torch.nn.init.xavier_uniform_(m.weight)
        m.bias.data.fill_(0.01)

def pred_per_epoch(num,X_test,st='soft'):
    model = ConvNet(1e-02,'mnist',torch.device('cpu'),10)
    model.load_state_dict(torch.load('mnist/model_100_11000_{}_{}_min.pth'.format(num,st)))
    pred=model.predict_proba(X_test)
    labels=np.argmax(pred,axis=1).reshape((-1,1)).astype(np.int8)
    return pred,labels

def KLD_loss(y_true: torch.Tensor, y_pred: torch.Tensor) -> torch.Tensor:
    """Compute the KLD Loss D_KL(y_true || y_pred) for binary classification
    with soft labels as Y_train

    Args:
        y_true (torch.Tensor): Y_train in form of soft labels
        y_pred (torch.Tensor): probabilities output by a binary classifier

    Returns:
        torch.Tensor: KLD Loss (with requires_grad=True for gradient descent)
    """
    aux = torch.unique(y_true)
    losses = []
    if 0 in aux:
        losses.append(torch.log(1/(1-y_pred[y_true==0])))
    if 1 in aux:
        losses.append(torch.log(1/(y_pred[y_true==1])))
    y_int = (1000*y_true.clone()).to(torch.int)
    float_index = torch.logical_and(y_int !=0,y_int!=1000)
    if len(float_index)!=0:
        losses.append(y_true[float_index]* torch.log(y_true[float_index]/y_pred[float_index]) + (1-y_true[float_index])*torch.log((1-y_true[float_index])/(1-y_pred[float_index])))
    #loss = F.kl_div(y_true,y_pred,reduction='mean')
    if torch.isnan(torch.mean(torch.cat(tuple(losses),0))):
        pass
    return torch.mean(torch.cat(tuple(losses),0))

class MyDataset(torch.utils.data.Dataset):
    """Custom dataset for pytorch model
    """
    def __init__(self, x_train: pd.DataFrame, y_train: pd.Series, credal: bool=False):
        """_summary_

        Args:
            x_train (pd.DataFrame): training features
            y_train (pd.Series): training label
            credal (bool, optional): if y_train is credal (i.e an interval)
                or not. Defaults to False.
        """
        
        x=x_train.values if type(x_train)==pd.core.frame.DataFrame else x_train
        y=y_train.values if type(y_train)==pd.core.frame.DataFrame else y_train
        self.X_train=torch.tensor(x,dtype=torch.float32)
        if not credal:
            self.Y_train=y.astype("float32")
        else:
            self.Y_train=y
    def __len__(self) -> int:
        """Get length

        Returns:
            int: length of y_train
        """
        return len(self.Y_train)
   
    def __getitem__(self, idx: int):
        """Get item

        Args:
            idx (int): indice of batch

        Returns:
            batch of X_train and corresponding batch of Y_train
        """
        return self.X_train[idx], self.Y_train[idx]


class ConvNet(nn.Module):
    def __init__(self,clipping_value: float,dataset:float,device,num_classes=10):
        if dataset == 'mnist':
            self.channels = 1
            self.num = 320
        else:
            self.channels = 3
            self.num = 500
        super(ConvNet, self).__init__()
        self.device = device
        self.conv1 = nn.Conv2d(self.channels, 10, kernel_size=5)#.to(self.device)
        self.conv2 = nn.Conv2d(10, 20, kernel_size=5)#.to(self.device)
        self.conv2_drop = nn.Dropout2d()#.to(self.device)
        self.fc1 = nn.Linear(self.num, 50)#.to(self.device)
        self.fc2 = nn.Linear(50, 10)#.to(self.device)
        self.num_classes=num_classes
        self.clipping_value=clipping_value
        self.losses = []
        self.accuracies = {}
    def forward(self, x):
        x = F.relu(F.max_pool2d(self.conv1(x), 2))
        x = F.relu(F.max_pool2d(self.conv2_drop(self.conv2(x)), 2))
        x = x.view(-1, self.num)
        x = F.relu(self.fc1(x))
        x = F.dropout(x, training=self.training)
        x = self.fc2(x)
        return F.softmax(x,dim=1)
    def __initiate_loss_and_accuracy_dicts(self, n_epochs: int):
        """initiate loss and accuracy dictionnaries to keep track of performances

        Args:
            n_epochs (int): number of epochs during learning
        """
        for i in range(n_epochs):
            self.losses[i] = 0
            self.accuracies[i] = 0

    def fit(
        self,
        x_train: pd.DataFrame,
        y_train: pd.Series,
        epochs: int,
        learning_rate: float,
        verbose=True,
        soft=False
    ):
        """Fit the neural network to the data

        Args:
            x_train (pd.DataFrame): Learning features
            y_train (pd.Series): Learning labels
            epochs (int): number of epochs for training
            learning_rate (float): learning rate for gradient descent
            verbose (bool, optional): Print the training informations as tensorflow does.
                Defaults to True.
            soft (bool, optional): True if y_train is composed of soft labels, else False.
                Defaults to False.
        """
        #self.__initiate_loss_and_accuracy_dicts(n_epochs=epochs)
        if self.channels == 1:
            myDs=MyDataset(x_train=np.expand_dims(x_train,axis=1), y_train=y_train)
        else:
            myDs=MyDataset(x_train=x_train.reshape((x_train.shape[0],self.channels,x_train.shape[1],x_train.shape[2])), y_train=y_train)
        train_loader=torch.utils.data.DataLoader(myDs,batch_size=10,shuffle=False)
        optimizer = torch.optim.Adam(self.parameters(),lr=learning_rate,weight_decay=1e-04)
        scheduler = lr_scheduler.CosineAnnealingWarmRestarts(optimizer, 7, 16)
        for epoch in range(epochs):
            n_batches = len(train_loader)
            if verbose:
                print(f'Epoch {epoch+1}/{epochs}')
                pbar = tf.keras.utils.Progbar(target=n_batches)
            for idx, batch in enumerate(train_loader):
                x, y = batch
                x = x#.to(self.device)
                y = y#.to(self.device)
                optimizer.zero_grad()
                outputs = self(x)
                outputs = torch.where(outputs>0.999, outputs-0.001, outputs)
                outputs = torch.where(outputs<0.001, outputs+0.001, outputs)
                
                y = torch.where(y>0.999, y-0.001, y)
                y = torch.where(y<0.001, y+0.001, y)
                
                if self.num_classes>2:
                    loss_value = (y*(y.log()-outputs.log())).sum(dim=1).mean()
                else:
                    labels = torch.argmax(y,dim=1).float()
                    if not soft:
                        loss = nn.BCELoss()
                        loss_value = loss(outputs, labels.reshape(-1, 1))
                    else:
                        loss_value = KLD_loss(y[:,1].reshape(-1, 1), outputs)
                #loss_value = loss(y_one.float(),outputs) #KLD_loss(y, outputs)
                torch.nn.utils.clip_grad_norm_(self.parameters(), self.clipping_value)
                loss_value.backward()
                optimizer.step()
                scheduler.step()
                if self.num_classes>2:
                    accuracy = accuracy_score(
                        y_true=torch.argmax(y,dim=1).cpu().detach().numpy(),
                        y_pred=torch.argmax(outputs,dim=1).cpu().detach().numpy()
                    )
                else:
                    accuracy = accuracy_score(
                        y_true=torch.argmax(y,dim=1).cpu().detach().numpy(),
                        y_pred=(outputs>0.5).detach().cpu().numpy().astype(int)
                    )
                #self.accuracies[epoch] += accuracy
                self.losses.append(loss_value.detach().item())#self.losses[epoch] += loss_value.detach().numpy()
                if verbose:
                    pbar.update(
                        idx,
                        values=[
                            ("loss", loss_value.detach().numpy()),
                            ("accuracy", accuracy)
                        ]
                    )
            #self.losses[epoch] = self.losses[epoch]/n_batches
            #self.accuracies[epoch] = self.accuracies[epoch]/n_batches
            if verbose:
                pbar.update(n_batches, values=None)

    def predict_proba(
        self,
        x: torch.Tensor,
        as_numpy=True,
        from_numpy=True
    ) -> np.ndarray:
        """Compute forward propagation after training to obtain probabilities P(Y=1|X=x)

        Args:
            x (Union[pd.DataFrame, torch.Tensor]): Input features
            as_numpy (bool, optional): If the output type need to be a numpy array.
                Defaults to False.
            from_numpy (bool, optional): If the input features are of type numpy.ndarray.
                Defaults to True.

        Returns:
            Union[torch.Tensor, np.ndarray]: probabilities P(Y=1|X=x) estimated by the
                neural network
        """
        if self.channels == 1:
            x= np.expand_dims(x,axis=1)
        else:
            x=x.reshape((x.shape[0],self.channels,x.shape[1],x.shape[2]))
        if from_numpy:
            x = torch.tensor(x, dtype=torch.float32).to(self.device)
        outputs = self(x)
        
        if as_numpy:
            outputs = outputs.cpu().detach().numpy()
            if self.num_classes==2:
                outputs = np.hstack((1-outputs,outputs))
        return outputs


def one_hot(vector,num_classes=3):
    vector = vector.squeeze()
    b = np.zeros((vector.size,num_classes)).astype(np.int64)
    b[np.arange(vector.size), vector] = 1
    return b

class SimpleNeuralNet(nn.Module):
    """Simple Neural Network (with 1 hidden layer) used for tests.
    Only use for binary classification and for hard or soft labels
    """
    def __init__(self, clipping_value: float, n_input_units: int, n_hidden_units: int,num_classes=10):
        """Initialisation of the neural network

        Args:
            clipping_value (float): clipping value to avoid exploding gradient
            n_input_units (int): number of neurons for the input layer
            n_hidden_units (int): number of neurons for the hidden layer
        """
        super().__init__()
        self.num_classes = num_classes
        self.input_layer = nn.Sequential(
            OrderedDict(
                {
                    'linear': nn.Linear(in_features=n_input_units, out_features=n_hidden_units),
                     'relu': nn.ReLU(inplace=True),
                }
            )
        )
        torch.nn.init.uniform_(self.input_layer[0].weight)
        torch.nn.init.uniform_(self.input_layer[0].bias)
        if self.num_classes>2:
            self.hidden_layer = nn.Linear(in_features=n_hidden_units, out_features=num_classes)       
            self.output_layer = nn.Softmax(dim=1)
        else:
            self.hidden_layer = nn.Linear(in_features=n_hidden_units, out_features=1)
            self.output_layer = nn.Sigmoid()
        torch.nn.init.uniform_(self.hidden_layer.weight)
        torch.nn.init.uniform_(self.input_layer[0].bias)
        self.losses = []
        self.accuracies = {}
        self.clipping_value = clipping_value

    def forward(self, x_train: torch.Tensor) -> torch.Tensor:
        """Computes the forward propagation

        Args:
            x (torch.Tensor): Features used

        Returns:
            torch.Tensor: probabilities output by the neural network
        """
        y_pred = self.input_layer(x_train)
        y_pred = self.hidden_layer(y_pred)      
        y_pred = self.output_layer(y_pred)
        return y_pred

    def __initiate_loss_and_accuracy_dicts(self, n_epochs: int):
        """initiate loss and accuracy dictionnaries to keep track of performances

        Args:
            n_epochs (int): number of epochs during learning
        """
        for i in range(n_epochs):
            self.losses[i] = 0
            self.accuracies[i] = 0

    def fit(
        self,
        x_train: pd.DataFrame,
        y_train: pd.Series,
        epochs: int,
        learning_rate: float,
        verbose=True,
        soft=False
    ):
        """Fit the neural network to the data

        Args:
            x_train (pd.DataFrame): Learning features
            y_train (pd.Series): Learning labels
            epochs (int): number of epochs for training
            learning_rate (float): learning rate for gradient descent
            verbose (bool, optional): Print the training informations as tensorflow does.
                Defaults to True.
            soft (bool, optional): True if y_train is composed of soft labels, else False.
                Defaults to False.
        """
        #self.__initiate_loss_and_accuracy_dicts(n_epochs=epochs)
        myDs=MyDataset(x_train=x_train, y_train=y_train)
        train_loader=torch.utils.data.DataLoader(myDs,batch_size=10,shuffle=False)
        optimizer = torch.optim.Adam(self.parameters(),lr=learning_rate,weight_decay=1e-04)
        scheduler = lr_scheduler.CosineAnnealingWarmRestarts(optimizer, 7, 16)
        for epoch in range(epochs):
            n_batches = len(train_loader)
            if verbose:
                print(f'Epoch {epoch+1}/{epochs}')
                pbar = tf.keras.utils.Progbar(target=n_batches)
            for idx, batch in enumerate(train_loader):
                x, y = batch
                optimizer.zero_grad()
                outputs = self(x)
                outputs = torch.where(outputs>0.999, outputs-0.001, outputs)
                outputs = torch.where(outputs<0.001, outputs+0.001, outputs)
                
                y = torch.where(y>0.999, y-0.001, y)
                y = torch.where(y<0.001, y+0.001, y)
                
                if self.num_classes>2:
                    loss_value = (y*(y.log()-outputs.log())).sum(dim=1).mean()
                    self.losses.append(loss_value)
                else:
                    labels = torch.argmax(y,dim=1).float()
                    if not soft:
                        loss = nn.BCELoss()
                        loss_value = loss(outputs, labels.reshape(-1, 1))
                    else:
                        loss_value = KLD_loss(y[:,1].reshape(-1, 1), outputs)
                #loss_value = loss(y_one.float(),outputs) #KLD_loss(y, outputs)
                torch.nn.utils.clip_grad_norm_(self.parameters(), self.clipping_value)
                loss_value.backward()
                optimizer.step()
                scheduler.step()
                if self.num_classes>2:
                    accuracy = accuracy_score(
                        y_true=torch.argmax(y,dim=1).detach().numpy(),
                        y_pred=torch.argmax(outputs,dim=1).detach().numpy()
                    )
                else:
                    accuracy = accuracy_score(
                        y_true=torch.argmax(y,dim=1).detach().numpy(),
                        y_pred=(outputs>0.5).detach().numpy().astype(int)
                    )
                #self.accuracies[epoch] += accuracy
                self.losses.append(loss_value.detach().item())#self.losses[epoch] += loss_value.detach().numpy()
                if verbose:
                    pbar.update(
                        idx,
                        values=[
                            ("loss", loss_value.detach().numpy()),
                            ("accuracy", accuracy)
                        ]
                    )
            #self.losses[epoch] = self.losses[epoch]/n_batches
            #self.accuracies[epoch] = self.accuracies[epoch]/n_batches
            if verbose:
                pbar.update(n_batches, values=None)
        #plt.plot(self.losses[(-1)*epochs:])
        #plt.savefig('{}/result_{}_{}_min'.format(par.dataset,par.rep_per_class*num_classes,par.split_size))
    def predict_proba(
        self,
        x: torch.Tensor,
        as_numpy=True,
        from_numpy=True
    ) -> np.ndarray:
        """Compute forward propagation after training to obtain probabilities P(Y=1|X=x)

        Args:
            x (Union[pd.DataFrame, torch.Tensor]): Input features
            as_numpy (bool, optional): If the output type need to be a numpy array.
                Defaults to False.
            from_numpy (bool, optional): If the input features are of type numpy.ndarray.
                Defaults to True.

        Returns:
            Union[torch.Tensor, np.ndarray]: probabilities P(Y=1|X=x) estimated by the
                neural network
        """
        if from_numpy:
            x = torch.tensor(x, dtype=torch.float32)
        outputs = self(x)
        if as_numpy:
            outputs = outputs.detach().numpy()
            if self.num_classes==2:
                outputs = np.hstack((1-outputs,outputs))
        return outputs
    


class PairwiseNeuralNet(nn.Module):
    """Simple Neural Network (with 1 hidden layer) used for tests.
    Only use for binary classification and for hard or soft labels
    """
    def __init__(self, clipping_value: float, n_input_units: int, n_hidden_units: int,num_classes=10):
        """Initialisation of the neural network

        Args:
            clipping_value (float): clipping value to avoid exploding gradient
            n_input_units (int): number of neurons for the input layer
            n_hidden_units (int): number of neurons for the hidden layer
        """
        super().__init__()
        self.models = nn.ParameterDict()
        self.num_classes = num_classes
        for i in range(num_classes-1):
            for j in range(num_classes):
                if j>i:
                    self.models[str(i)+str(j)] =  SimpleNeuralNet(clipping_value,n_input_units, n_hidden_units,2)
        self.losses = []
        self.clipping_value = clipping_value

    def forward(self,X,as_numpy,from_numpy):
        return self.predict_proba(X,as_numpy=as_numpy,from_numpy=from_numpy)
    
    def fit(self,
        x_train: pd.DataFrame,
        y_train: pd.Series,
        epochs: int,
        learning_rate: float,
        verbose=True,
        soft=False):

        #self.__initiate_loss_and_accuracy_dicts(n_epochs=epochs)
        myDs=MyDataset(x_train=x_train, y_train=y_train)
        torch.autograd.set_detect_anomaly(True)
        train_loader=torch.utils.data.DataLoader(myDs,batch_size=10,shuffle=False)
        optimizer = torch.optim.Adam(self.parameters(),lr=learning_rate,weight_decay=1e-04)
        scheduler = lr_scheduler.CosineAnnealingWarmRestarts(optimizer, 7, 16)
        for epoch in range(epochs):
            n_batches = len(train_loader)
            if verbose:
                print(f'Epoch {epoch+1}/{epochs}')
                pbar = tf.keras.utils.Progbar(target=n_batches)
            for idx, batch in enumerate(train_loader):
                x, y = batch
                optimizer.zero_grad()
                outputs = self(x,from_numpy=False,as_numpy=False)
                outputs = torch.where(outputs>0.999, outputs-0.001, outputs)
                outputs = torch.where(outputs<0.001, outputs+0.001, outputs)
                
                y = torch.where(y>0.999, y-0.001, y)
                y = torch.where(y<0.001, y+0.001, y)
                
                if self.num_classes>2:
                    loss_value = (y*(y.log()-outputs.log())).sum(dim=1).mean()
                    self.losses.append(loss_value)
                else:
                    labels = torch.argmax(y,dim=1).float()
                    if not soft:
                        loss = nn.BCELoss()
                        loss_value = loss(outputs, labels.reshape(-1, 1))
                    else:
                        loss_value = KLD_loss(y[:,1].reshape(-1, 1), outputs)
                #loss_value = loss(y_one.float(),outputs) #KLD_loss(y, outputs)
                torch.nn.utils.clip_grad_norm_(self.parameters(), self.clipping_value)
                loss_value.backward()
                optimizer.step()
                scheduler.step()
                if self.num_classes>2:
                    accuracy = accuracy_score(
                        y_true=torch.argmax(y,dim=1).detach().numpy(),
                        y_pred=torch.argmax(outputs,dim=1).detach().numpy()
                    )
                else:
                    accuracy = accuracy_score(
                        y_true=torch.argmax(y,dim=1).detach().numpy(),
                        y_pred=(outputs>0.5).detach().numpy().astype(int)
                    )
                self.losses.append(loss_value.detach().item())#self.losses[epoch] += loss_value.detach().numpy()
                if verbose:
                    pbar.update(
                        idx,
                        values=[
                            ("loss", loss_value.detach().numpy()),
                            ("accuracy", accuracy)
                        ]
                    )
            if verbose:
                pbar.update(n_batches, values=None)
        '''
        for i in range(self.num_classes-1):
            for j in range(self.num_classes):
                if j > i:
                    labels = np.argmax(y_train,axis=1)
                    y1 = np.hstack((y_train[:,i].reshape((-1,1)),y_train[:,j].reshape((-1,1))))
                    indexes = []
                    for ii in range(len(y1)):
                        if np.sum(y1[ii])==0:
                            indexes.append(ii)
                        else:
                            y1[ii] = y1[ii]/np.sum(y1[ii])
                    y1 = np.delete(y1,indexes,0)
                    train = np.delete(x_train,indexes,0)
                    self.models[str(i)+str(j)].fit(train,y1,epochs,learning_rate,verbose,soft)
        '''
    def pairwise_proba(self,X,from_numpy):
        aux = torch.zeros((X.shape[0],self.num_classes,self.num_classes))-1
        epsilon = 0.005
        for i in range(self.num_classes):
            for j in range(self.num_classes):
                if j > i:
                    aux[:,i,j]=self.models[str(i)+str(j)].predict_proba(X,as_numpy=False,from_numpy=from_numpy).squeeze()
                    aux[:,j,i]= 1-aux[:,i,j]

        aux = torch.where(aux==0,aux+epsilon,aux)#avoid division by zero
        return aux
    
    def predict_proba(self,X,from_numpy,as_numpy=True):
        out = self.pkpd(self.pairwise_proba(X,from_numpy=from_numpy))
        if as_numpy:
            return out.detach().numpy()
        return out
    
    def pkpd(self,pairwise_prob,normalize=True):
        prob_vector = torch.zeros((pairwise_prob.shape[0],self.num_classes))
        
        for ii in range(prob_vector.shape[0]):
            for jj in range(self.num_classes):
                sum_vector = torch.reciprocal(pairwise_prob[ii,jj])
                sum_vector = torch.sum(sum_vector)+1
                prob_vector[ii,jj] = 1/(sum_vector-self.num_classes+2)
            #if normalize:
            #    prob_vector[ii] = prob_vector[ii]/torch.sum(prob_vector[ii])
        return prob_vector
    
    def multi_class_IVAP(self,calibration_features,calibration_labels,test_instance):
        aux = np.zeros((self.num_classes,self.num_classes,2)) -1
        output = np.zeros((self.num_classes,self.num_classes-1,2))
        for i in range(self.num_classes):
            for j in range(self.num_classes):
                if j > i:
                    #aux = np.vstack((y_train[labels==i],y_train[labels==j]))
                    y1 = np.hstack((calibration_labels[:,i].reshape((-1,1)),calibration_labels[:,j].reshape((-1,1))))
                    indexes = []
                    
                    for ii in range(len(y1)):
                        if np.sum(y1[ii])==0:
                            indexes.append(ii) #y1[ii] = 0.5*np.ones(2)
                        else:
                            y1[ii] = y1[ii]/np.sum(y1[ii])
                    y1 = np.delete(y1,indexes,0)
                    train = np.delete(calibration_features,indexes,0)
                    aux[i,j] = venn_abers_pytorch(self.models[str(i)+str(j)], 
                            torch.from_numpy(train.astype(np.float32)),
                            torch.from_numpy(y1),
                            torch.from_numpy(test_instance.astype(np.float32)))
                    aux[j,i,0] = 1-aux[i,j,1]
                    aux[j,i,1] = 1-aux[i,j,0]
                    
        output = aux[aux!=-1].reshape((self.num_classes,self.num_classes-1,2))
        #lower = self.pkpd(output[:,:,0].reshape((1,self.num_classes,self.num_classes-1)),normalize=False).flatten()
        #upper  =self.pkpd(output[:,:,1].reshape((1,self.num_classes,self.num_classes-1)),normalize=False).flatten()
        return aux
class PairwiseNeuralNetOVA(nn.Module):
    """Simple Neural Network (with 1 hidden layer) used for tests.
    Only use for binary classification and for hard or soft labels
    """
    def __init__(self, clipping_value: float, n_input_units: int, n_hidden_units: int,device,num_classes=10,opt='NN'):
        """Initialisation of the neural network

        Args:
            clipping_value (float): clipping value to avoid exploding gradient
            n_input_units (int): number of neurons for the input layer
            n_hidden_units (int): number of neurons for the hidden layer
        """
        super().__init__()
        self.device = device
        self.num_classes = num_classes
        self.opt = opt
        self.mod = torch
        self.models = nn.ParameterDict()
        for i in range(num_classes):
                self.models[str(i)] = SimpleNeuralNet(clipping_value,n_input_units, n_hidden_units,2).to(device)
                '''nn.Sequential( 
                        nn.Linear(in_features=n_input_units,out_features=n_hidden_units),
                        nn.ReLU(inplace=True),
                        nn.Linear(in_features=n_hidden_units,out_features=1),
                        #nn.Dropout(p=0.5),
                        nn.Sigmoid()).to(device) #simple Logistic Regression'''
        self.losses = []
        self.clipping_value = clipping_value

    def forward(self,X,as_numpy=True):
        return self.predict_proba(X,as_numpy=as_numpy)
    
    def fit(self,
        x_train: pd.DataFrame,
        y_train: pd.Series,
        epochs: int,
        learning_rate: float,
        verbose=True,
        soft=True):
        x_train = self._tonumpy(x_train)
        for i in range(self.num_classes):
                y1 = np.expand_dims(y_train[:,i].copy(),axis=-1)
                y1 = self._tonumpy(y1)
                y1 = np.hstack((1-y1,y1))
                self.models[str(i)].fit(x_train,y1,epochs,learning_rate,verbose,soft)
        #self.__initiate_loss_and_accuracy_dicts(n_epochs=epochs)
        
    def _tonumpy(self,x):
        if self.opt == 'LR':
            return x.detach().cpu().numpy()
        return x
    
    def predict_proba(self,X,as_numpy=True):
        if not torch.is_tensor(X):
            X = torch.from_numpy(X).to(self.device).type(torch.float32)
        aux = torch.zeros((X.shape[0],self.num_classes),dtype=torch.float32)
        for i in range(self.num_classes):        
            aux[:,i]=self.models[str(i)](X).squeeze()
        if as_numpy:
            return aux.detach().cpu().numpy()
        return aux.to(self.device)
    
    def multi_class_IVAP(self,calibration_features,calibration_labels,test_instance):
        aux = np.zeros((self.num_classes,2))
        for i in range(self.num_classes):
            #aux = np.vstack((y_train[labels==i],y_train[labels==j]))
            y1 =np.expand_dims(calibration_labels[:,i].copy(),axis=1)
            y1 = np.hstack((1-y1,y1))
            self.models[str(i)] = self.models[str(i)].cpu()
            calibrations = self.models[str(i)](torch.from_numpy(calibration_features).type(torch.float32)).detach().numpy()#.predict_proba(calibration_features)
            predictions = self.models[str(i)](torch.from_numpy(test_instance).type(torch.float32)).detach().numpy()#.predict_proba(test_instance)
            self.models[str(i)]= self.models[str(i)].to(self.device)
            aux[i] = venn_abers_pytorch(calibrations,torch.from_numpy(y1),predictions)
        return aux