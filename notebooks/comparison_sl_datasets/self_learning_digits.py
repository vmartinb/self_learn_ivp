#!/usr/bin/env python
# coding: utf-8

# In[1]:


import sys
sys.path.append('/home/vitorbordini/Downloads/self_learn_ivp/')
import pandas as pd
import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.isotonic import IsotonicRegression
from sklearn.datasets import load_digits
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
import seaborn as sns

import torch
from torch import nn
from torch.autograd import Variable
from collections import OrderedDict
import tensorflow as tf

from neural_net import SimpleNeuralNet, SimpleNeuralNetCredal
from utils import MyDataset, plot_decision_boundary


# In[2]:


fsize = 15
tsize = 18
tdir = 'in'
major = 5.0
minor = 3.0
lwidth = 0.8
lhandle = 2.0
plt.style.use('default')
#plt.rcParams['text.usetex'] = True
plt.rcParams['font.size'] = fsize
plt.rcParams['legend.fontsize'] = tsize
plt.rcParams['xtick.direction'] = tdir
plt.rcParams['ytick.direction'] = tdir
plt.rcParams['xtick.major.size'] = major
plt.rcParams['xtick.minor.size'] = minor
plt.rcParams['ytick.major.size'] = 5.0
plt.rcParams['ytick.minor.size'] = 3.0
plt.rcParams['axes.linewidth'] = lwidth


# In[3]:


df = load_digits(as_frame=True).frame
df.head()


# In[4]:


scale = StandardScaler()
columns = df.columns
y = pd.get_dummies(df["target"].copy())
df = pd.DataFrame(scale.fit_transform(df))
df.columns = columns
df.head()


# In[5]:


y


# In[6]:


X_train, X_test, Y_train, Y_test = train_test_split(df.drop("target", axis=1), y, train_size=0.8, random_state=235)
X_train, X_calib, Y_train, Y_calib = train_test_split(X_train, Y_train, train_size=0.95, random_state=235)
trainset = pd.DataFrame(X_train, columns=df.drop("target", axis=1).columns)
y.iloc[80:] = np.NaN 
known_train = trainset.iloc[0:80]
unknow_train = trainset.iloc[80:]
unknow_labels = y.iloc[80:]
known_labels = y.iloc[0:80]
known_train.shape, unknow_train.shape,known_labels.shape,unknow_labels.shape, X_calib.shape, X_test.shape


# # Self Learning strategy 1

# In[7]:


class SelfLearning():
    def __init__(
        self,
        known_x_train,
        known_y_train,
        unknown_x_train,
        model_convergence_epochs,
        verbose
    ):
        self.model_convergence_epochs = model_convergence_epochs
        torch.manual_seed(10)
        self.model = SimpleNeuralNet(clipping_value=0.01, n_input_units=64, n_hidden_units=10)
        self.known_x_train = known_x_train
        self.known_y_train = known_y_train
        self.unknown_x_train = unknown_x_train
        self.unknown_y_train = pd.DataFrame(0, 
                                            index=range(unknown_x_train.shape[0]), 
                                            columns=range(unknown_x_train.shape[1]))
        
        self.model.fit(self.known_x_train, self.known_y_train, epochs=self.model_convergence_epochs, learning_rate=0.01, verbose=verbose)
        self.accuracies = []
    
    def learning(self, validation_x, validation_y, batch_adding=5):
        while len(self.unknown_x_train) >= batch_adding:
            self.unknown_y_train = pd.DataFrame(self.model.predict_probas(self.unknown_x_train.to_numpy()).detach().numpy())
            #self.unknown_x_train.sort_values("y", ascending=False, inplace=True)
            self.unknown_y_train = self.unknown_y_train.applymap(lambda x: 1 if x>0.5 else 0)
            self.known_x_train = pd.concat(
                [
                    self.known_x_train,
                    self.unknown_x_train.iloc[0:batch_adding],
                    self.unknown_x_train.iloc[-batch_adding:]
                ]
            )
            self.known_y_train = pd.concat(
                [
                    self.known_y_train,
                    self.unknown_y_train.iloc[0:batch_adding],
                    self.unknown_y_train.iloc[-batch_adding:]
                ]
            )
            self.unknown_x_train = self.unknown_x_train.iloc[batch_adding:-batch_adding]
            predictions = self.model.predict_probas(validation_x.to_numpy()).detach().numpy().round()
            self.accuracies.append(accuracy_score(validation_y, predictions))
            self.model.fit(self.known_x_train, self.known_y_train, epochs=self.model_convergence_epochs, learning_rate=0.05, verbose=False)
        self.unknown_y_train = pd.DataFrame(self.model.predict_probas(self.unknown_x_train.to_numpy()).detach().numpy())
        self.unknown_y_train = self.unknown_y_train.applymap(lambda x: 1 if x>0.5 else 0)
        self.known_x_train = pd.concat(
            [
                self.known_x_train,
                self.unknown_x_train,
            ]
        )
        self.known_y_train = pd.concat(
            [
                self.known_y_train,
                self.unknown_y_train,
            ]
        )
        self.model.fit(self.known_x_train, self.known_y_train, epochs=self.model_convergence_epochs, learning_rate=0.01, verbose=False)
        predictions = self.model.predict_probas(validation_x.to_numpy()).detach().numpy().round()
        self.accuracies.append(accuracy_score(validation_y, predictions))


# In[8]:


test1 = SelfLearning(
    known_x_train=known_train,
    known_y_train=known_labels,
    unknown_x_train=unknow_train,
    model_convergence_epochs=50,
    verbose=True
)
test1.learning(validation_x=X_test, validation_y=Y_test, batch_adding=20)
sns.lineplot(x=[i for i in range(len(test1.accuracies))], y=test1.accuracies).set(title="Accuracy")
plt.show()


# # Self Learning strategy 2

# In[9]:


class SelfLearningWithSoft():
    def __init__(
        self,
        known_x_train,
        known_y_train,
        unknown_x_train,
        model_convergence_epochs,
        verbose
    ):
        self.model_convergence_epochs = model_convergence_epochs
        torch.manual_seed(3)
        self.model = SimpleNeuralNet(clipping_value=0.01, n_input_units=64, n_hidden_units=10)
        self.known_x_train = known_x_train
        self.known_y_train = known_y_train
        self.unknown_x_train = unknown_x_train
        self.length_known = len(self.known_y_train)
        self.unknown_y_train = pd.DataFrame(0, 
                                            index=range(unknown_x_train.shape[0]), 
                                            columns=range(unknown_x_train.shape[1]))
        
        self.model.fit(self.known_x_train, self.known_y_train, epochs=self.model_convergence_epochs, learning_rate=0.01, verbose=verbose)
        self.accuracies = []
    
    def learning(self, validation_x, validation_y, n_epochs=10, verbose=True):
        for epochs in range(n_epochs):
            self.known_y_train = self.known_y_train.applymap(lambda x: x-0.001 if x==1 else x+0.001 if x==0 else x)
            self.unknown_y_train = pd.DataFrame(self.model.predict_probas(self.unknown_x_train.to_numpy()).detach().numpy())
            self.known_x_train = pd.concat(
                [
                    self.known_x_train,
                    self.unknown_x_train,
                ]
            ).drop_duplicates()
            if epochs == 0:
                self.known_y_train = pd.concat(
                    [
                        self.known_y_train,
                        self.unknown_y_train,
                    ]
                )
            else:
                self.known_y_train[self.length_known:] = self.unknown_y_train
            self.unknown_x_train = self.unknown_x_train
            predictions = pd.DataFrame(self.model.predict_probas(validation_x.to_numpy()).detach().numpy().round())
            self.accuracies.append(accuracy_score(validation_y, predictions))
            if verbose:
                print(f"Accuracy epochs {epochs}: {accuracy_score(validation_y, predictions)}")
            self.model.fit(
                self.known_x_train, self.known_y_train, epochs=self.model_convergence_epochs, learning_rate=0.01, verbose=False, soft=True
            )


# In[ ]:


test2 = SelfLearningWithSoft(
    known_x_train=known_train,
    known_y_train=known_labels,
    unknown_x_train=unknow_train,
    model_convergence_epochs=10,
    verbose=True
)
test2.learning(validation_x=X_test, validation_y=Y_test, n_epochs=10)
sns.lineplot(x=[i for i in range(len(test2.accuracies))], y=test2.accuracies).set(title="Accuracy")
plt.show()


# # Self Learning strategy 3

# In[11]:


def venn_abers_pytorch(trained_classifier, calibration_features, test_instance, calibration_labels):
    calibrations = trained_classifier.predict_probas(calibration_features, as_numpy=True, from_numpy=False)
    predictions = trained_classifier.predict_probas(test_instance, as_numpy=True, from_numpy=False)
    interval = []
    scores = pd.DataFrame()
    scores["s"] = calibrations[:, 0]
    scores["y"] = calibration_labels.to_numpy()
    score = pd.DataFrame()
    score["s"] = predictions[:, 0]
    interval = [] 
    
    for i in [0, 1]:
        score["y"] = i
        train = pd.concat([scores, score], ignore_index=True)
        g = IsotonicRegression(y_min=0.001, y_max=0.999, out_of_bounds='clip')
        g.fit(train["s"], train["y"])
        pred = g.predict(score["s"])[0]
        if pred == np.nan:
            print("PROBA PREDICTED: ", predictions)
            print(g.fit_transform(train["s"], train["y"]))
        interval.append(g.predict(score["s"])[0])
    interval = np.array(interval)
    return interval


# In[12]:


class SelfLearningUsingVennAbers():
    def __init__(
        self,
        known_x_train,
        known_y_train,
        unknown_x_train,
        calib_x_train, 
        calib_y_train,
        model_convergence_epochs,
        verbose
    ):
        self.model_convergence_epochs = model_convergence_epochs
        torch.manual_seed(3)
        self.model = SimpleNeuralNetCredal(clipping_value=0.01, n_input_units=64, n_hidden_units=10)
        self.known_x_train = known_x_train
        self.known_y_train = known_y_train
        self.unknown_x_train = unknown_x_train
        self.calib_x_train = calib_x_train
        self.calib_y_train = calib_y_train
        self.length_known = len(self.known_y_train)
        
        self.model.fit(self.known_x_train, self.known_y_train, epochs=self.model_convergence_epochs, learning_rate=0.01, verbose=verbose)
        self.accuracies = []
    
    def learning(self, validation_x, validation_y, n_epochs=10, verbose=True):
        self.known_y_train = self.known_y_train.apply(
            lambda x: np.stack(
                [x+0.001, x+0.001] if x == 0 else [x-0.001, x-0.001] if x ==1 else x,
                axis=-1
        ).astype(np.float32))
        for epochs in range(n_epochs):
            self.unknown_x_train["interval"] =  self.unknown_x_train.apply(
                lambda x: venn_abers_pytorch(
                    self.model,
                    torch.from_numpy(self.calib_x_train.values.astype(np.float32)),
                    torch.from_numpy(x.values.reshape(1, -1).astype(np.float32)),
                    self.calib_y_train)
                ,
                axis=1
            )
            self.known_x_train = pd.concat(
                [
                    self.known_x_train,
                    self.unknown_x_train.drop("interval", axis=1),
                ]
            ).drop_duplicates()
            if epochs == 0:
                self.known_y_train = pd.concat(
                    [
                        self.known_y_train,
                        self.unknown_x_train["interval"],
                    ]
                )
            else:
                self.known_y_train[self.length_known:] = self.unknown_x_train["interval"]
            
            self.unknown_x_train = self.unknown_x_train.drop("interval", axis=1)
            predictions = self.model.predict_probas(validation_x.to_numpy()).reshape(-1).detach().numpy().round()
            self.accuracies.append(accuracy_score(validation_y, predictions))
            if verbose:
                print(f"Accuracy epochs {epochs+1}: {accuracy_score(validation_y, predictions)}")
            self.model.fit(
                self.known_x_train,
                self.known_y_train,
                epochs=self.model_convergence_epochs,
                learning_rate=0.01,
                verbose=False,
                credal=True
            )
            
    
    def predict_probas(self, x_test):
        probs = self.model.predict_probas(x_test, from_numpy=True, as_numpy=True)
        return probs
    
    def predict_probas_interval(self, x_test):
        returns = x_test.copy()
        returns["interval"] = returns.apply(
                lambda x: venn_abers_pytorch(
                    self.model,
                    torch.from_numpy(self.calib_x_train.values.astype(np.float32)),
                    torch.from_numpy(x.values.reshape(1, -1).astype(np.float32)),
                    self.calib_y_train)
                ,
                axis=1
            )
        return returns


# In[13]:


test3 = SelfLearningUsingVennAbers(
    known_x_train=known_train.drop(["target"], axis=1),
    known_y_train=known_train["target"],
    unknown_x_train=unknow_train.drop(["target"], axis=1),
    calib_x_train=X_calib,
    calib_y_train=Y_calib,
    model_convergence_epochs=10,
    verbose=True
)
test3.learning(validation_x=X_test, validation_y=Y_test, n_epochs=33)
sns.lineplot(x=[i for i in range(len(test3.accuracies))], y=test3.accuracies).set(title="Accuracy")
plt.show()


# In[14]:


plt.figure(figsize=(20, 10))
sns.lineplot(x=[i for i in range(len(test1.accuracies))], y=test1.accuracies, markers=True, label="Self Learning")
sns.lineplot(x=[i for i in range(len(test2.accuracies))], y=test2.accuracies, markers=True, label="Self Learning using soft labels")
sns.lineplot(x=[i for i in range(len(test3.accuracies))], y=test3.accuracies, markers=True, label="Self Learning using Venn Abers")
plt.show()


# In[15]:


sl_acc = []
sl_sl_acc = []
sl_va_acc = []
for i in [90, 214, 235, 64, 27, 23, 305, 909, 654, 75]:
    df = load_digits(as_frame=True).frame
    df["target"] = df["target"].apply(lambda x: 0 if x%2==0 else 1)
    scale = StandardScaler()
    columns = df.columns
    y = df["target"].copy()
    df = pd.DataFrame(scale.fit_transform(df))
    df.columns = columns
    df["target"] = y.copy()
    
    X_train, X_test, Y_train, Y_test = train_test_split(df.drop("target", axis=1), df["target"], train_size=0.8, random_state=i)
    X_train, X_calib, Y_train, Y_calib = train_test_split(X_train, Y_train, train_size=0.95, random_state=i)
    trainset = pd.DataFrame(X_train, columns=df.drop("target", axis=1).columns)
    trainset["target"] = Y_train
    trainset.iloc[80:, trainset.columns.get_loc("target")] = np.NaN 
    known_train = trainset.iloc[0:80]
    unknow_train = trainset.iloc[80:]
    
    test1 = SelfLearning(
        known_x_train=known_train.drop(["target"], axis=1),
        known_y_train=known_train["target"],
        unknown_x_train=unknow_train.drop(["target"], axis=1),
        model_convergence_epochs=10,
        verbose=False
    )
    test1.learning(validation_x=X_test, validation_y=Y_test, batch_adding=20)
    
    test2 = SelfLearningWithSoft(
        known_x_train=known_train.drop(["target"], axis=1),
        known_y_train=known_train["target"],
        unknown_x_train=unknow_train.drop(["target"], axis=1),
        model_convergence_epochs=10,
        verbose=False
    )
    test2.learning(validation_x=X_test, validation_y=Y_test, n_epochs=33, verbose=False)
    
    test3 = SelfLearningUsingVennAbers(
        known_x_train=known_train.drop(["target"], axis=1),
        known_y_train=known_train["target"],
        unknown_x_train=unknow_train.drop(["target"], axis=1),
        calib_x_train=X_calib,
        calib_y_train=Y_calib,
        model_convergence_epochs=10,
        verbose=False
    )
    test3.learning(validation_x=X_test, validation_y=Y_test, n_epochs=33, verbose=False)
    plt.figure(figsize=(20, 10))
    sns.lineplot(x=[i for i in range(len(test1.accuracies))], y=test1.accuracies, markers=True, label="Self Learning")
    sns.lineplot(x=[i for i in range(len(test2.accuracies))], y=test2.accuracies, markers=True, label="Self Learning using soft labels")
    sns.lineplot(x=[i for i in range(len(test3.accuracies))], y=test3.accuracies, markers=True, label="Self Learning using Venn Abers")
    plt.show()
    
    sl_acc.append(test1.accuracies)
    sl_sl_acc.append(test2.accuracies)
    sl_va_acc.append(test3.accuracies)


# In[16]:


sl_acc_mean = []
sl_sl_acc_mean = []
sl_va_acc_mean = []

for i in range(30):
    sum_acc_sl = 0
    sum_acc_sl_sl = 0
    sum_acc_sl_va = 0
    for j in range(10):
        sum_acc_sl += sl_acc[j][i]
        sum_acc_sl_sl += sl_sl_acc[j][i]
        sum_acc_sl_va += sl_va_acc[j][i]
    sl_acc_mean.append(sum_acc_sl/10)
    sl_sl_acc_mean.append(sum_acc_sl_sl/10)
    sl_va_acc_mean.append(sum_acc_sl_va/10)


# In[17]:


plt.figure(figsize=(20, 10))
sns.lineplot(x=[i for i in range(1, len(sl_acc_mean)+1)], y=sl_acc_mean, markers=True, label="Self Learning")
sns.lineplot(x=[i for i in range(1, len(sl_sl_acc_mean)+1)], y=sl_sl_acc_mean, markers=True, label="Self Learning using soft labels")
sns.lineplot(x=[i for i in range(1, len(sl_va_acc_mean)+1)], y=sl_va_acc_mean, markers=True, label="Self Learning using Venn Abers")
plt.xlabel("Number of iterations")
plt.ylabel("Mean accuracy on test set")
plt.show()


# In[18]:


np.mean(sl_acc_mean), np.mean(sl_sl_acc_mean), np.mean(sl_va_acc_mean)


# In[19]:


sl_30_acc = []
sl_sl_30_acc = []
sl_va_30_acc = []

for i in range(10):
    sl_30_acc.append(sl_acc[i][29])
    sl_sl_30_acc.append(sl_sl_acc[i][29])
    sl_va_30_acc.append(sl_va_acc[i][29])

np.std(sl_30_acc), np.std(sl_sl_30_acc), np.std(sl_va_30_acc)


# In[20]:


sl_acc_mean[29], sl_sl_acc_mean[29], sl_va_acc_mean[29]


# In[ ]:




