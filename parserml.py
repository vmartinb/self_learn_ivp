import argparse

def argpar():
	parser = argparse.ArgumentParser(description="Compare different self-learning strategies")
	parser.add_argument("--model", default='fitnet',help="Which architecture to use")
#	parser.add_argument("--logdir", required=True,
#		      help="Where to log training info (small).")
	parser.add_argument("--dataset",default='ecoli',
		      help="Choose the dataset")
	parser.add_argument("--st",default='soft',choices = ["soft","credal","hard"],
		      help="Choose the strateggy")
	parser.add_argument("--rep_per_class", type=int, default=7,
		      help="use this many examples for calibration size"
		      "per class only.")
	parser.add_argument("--batch", type=int, default=25,
		      help="Batch size.")
	parser.add_argument("--split_size", type=float, default=0.2,
		      help="Number of labeled data.")
	parser.add_argument("--iteration_init", type=int, default=-1,
		      help="Initial Iteration")
	parser.add_argument("--seeds_num", type=int, default=5,
		      help="Number of seeds used")
	parser.add_argument("--p", type=float, default=0.25,
		      help="Noise proportion in a symmetric case")
	parser.add_argument("--sym_noise", action='store_false',
		      help="Whether noise is symmetric or not.")  
	parser.add_argument("--cuda", type=int,default=-1,
		      help="Use CUDA if possible")  
	parser.add_argument("--im_data", action='store_true',
		      help="Whether it is image set or not")
	
	return parser.parse_args()
